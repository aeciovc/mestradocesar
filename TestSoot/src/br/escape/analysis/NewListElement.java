package br.escape.analysis;

public class NewListElement {
	
    ListElement org; 
	NewListElement next;
	
	
	NewListElement(){
		org = null; next = null;
	}
	
	static void T(ListElement f1, ListElement f2){

		NewListElement r = new NewListElement(); 
		
		
		while(f1 != null){
			r.org = f1.next;

			r.next = new NewListElement();

			// do some computation using r
		  // w/o changing the data structure

			r = r.next;

			if(f1.data == 0){
				ListElement.g = f2;
			}

			f1 = f1.next;
		}
	}
}
