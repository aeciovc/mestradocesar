/* Soot - a J*va Optimization Framework
 * Copyright (C) 2008 Eric Bodden
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import soot.Body;
import soot.BodyTransformer;
import soot.G;
import soot.Local;
import soot.PackManager;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Transform;
import soot.Unit;
import soot.ValueBox;
import soot.toolkits.graph.DominatorsFinder;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.toolkits.graph.MHGDominatorsFinder;
import soot.toolkits.graph.UnitGraph;
import soot.toolkits.scalar.ArraySparseSet;
import soot.toolkits.scalar.BackwardFlowAnalysis;
import soot.toolkits.scalar.FlowSet;
import soot.toolkits.scalar.ForwardFlowAnalysis;

public class MyMainBackward {

	public static void main(String[] args) {
		
		SootClass c = Scene.v().loadClassAndSupport("Teste");
		c.setApplicationClass();
		
		SootMethod m = c.getMethodByName("teste");
		
		Body b = m.retrieveActiveBody();
		
		UnitGraph g = new ExceptionalUnitGraph(b);
		
		MyAnalysis an = new MyAnalysis(g);
		for (Unit unit : g) {
			FlowSet in = (FlowSet) an.getFlowBefore(unit);
			FlowSet out = (FlowSet) an.getFlowAfter(unit);
			
			G.v().out.println("in:::"+in.toString());
			G.v().out.println("out:::"+out.toString());
		}

		
	}

	public static class MyAnalysis extends BackwardFlowAnalysis {

		FlowSet emptySet = new ArraySparseSet();
		Map<Unit, FlowSet> unitToGenerateSet;

		public MyAnalysis(UnitGraph graph) {
			super(graph);
			G.v().out.println("testando...");
			
			
			DominatorsFinder df = new MHGDominatorsFinder(graph);
	        unitToGenerateSet = new HashMap<Unit, FlowSet>(graph.size() * 2 + 1, 0.7f);

	        // pre-compute generate sets
	        for(Iterator unitIt = graph.iterator(); unitIt.hasNext();){
	            Unit s = (Unit) unitIt.next();
	            FlowSet genSet = emptySet.clone();
	            
	            G.v().out.println("Unit:"+ s.toString());
	            
	            for(Iterator domsIt = df.getDominators(s).iterator(); domsIt.hasNext();){
	                Unit dom = (Unit) domsIt.next();
	                for(Iterator boxIt = dom.getDefBoxes().iterator(); boxIt.hasNext();){
	                    ValueBox box = (ValueBox) boxIt.next();
	                    if(box.getValue() instanceof Local)
	                        genSet.add(box.getValue(), genSet);
	                }
	            }
	            
	            //G.v().out.println("Putting::::::::::::::::::"+ s.toString() + " - "+ genSet.toString());
	            unitToGenerateSet.put(s, genSet);
	        }

			doAnalysis();
		}

		/**
		 * All INs are initialized to the empty set.
		 **/
		protected Object newInitialFlow() {
			FlowSet s = emptySet.clone();
			
			G.v().out.println("newInitialFlow... "+ emptySet.toString());
			return s;
		}

		/**
		 * IN(Start) is the empty set
		 **/
		protected Object entryInitialFlow() {
			G.v().out.println("entryInitialFlow..." + emptySet.toString());
			return emptySet.clone();
		}

		/**
		 * OUT is the same as IN plus the genSet.
		 **/
		protected void flowThrough(Object inValue, Object unit, Object outValue) {

			FlowSet in = (FlowSet) inValue, out = (FlowSet) outValue;

			// perform generation (kill set is empty)
			in.union(unitToGenerateSet.get(unit), out);
			
			G.v().out.println("FlowSet UNIT  :"+ unit.toString());
			G.v().out.println("FlowSet IN    :"+ in.toString());
			G.v().out.println("FlowSet OUT   :"+ out.toString());
		}

		/**
		 * All paths == Intersection.
		 **/
		protected void merge(Object in1, Object in2, Object out) {
			G.v().out.println("merge...:");

			FlowSet inSet1 = (FlowSet) in1, inSet2 = (FlowSet) in2, outSet = (FlowSet) out;
			
			inSet1.intersection(inSet2, outSet);
		}

		protected void copy(Object source, Object dest) {
			//G.v().out.println("copy...");
			FlowSet sourceSet = (FlowSet) source, destSet = (FlowSet) dest;

			/*G.v().out.println("Source: "+ sourceSet.toString());
			G.v().out.println("destSet: "+ destSet.toString());*/
			
		
			sourceSet.copy(destSet);
		}
		
/*		@Override
		public Object getFlowAfter(Object s) {
			
			FlowSet i = (FlowSet) s;
			G.v().out.println("getFlowAfter():"+i.toString());
			
			return super.getFlowAfter(s);
		}
*/
	}

}