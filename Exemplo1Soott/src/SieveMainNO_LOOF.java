

public class SieveMainNO_LOOF {


	static void teste() {
		String results1, results2;
		results1 = "Running Sieve benchmark.";
		results2 = "This will take about 10 seconds.";

		Integer SIZE = 8190;
		Boolean flags[] = new Boolean[SIZE + 1];
		Integer i, prime, k, iter, count;
		Integer iterations = 0;
		Double seconds = 0.0;
		Integer score = 0;
		Long startTime, elapsedTime;

		startTime = System.currentTimeMillis();
		while (true) {
			count = 0;
			for (i = 0; i <= SIZE; i++){
				//Insert free Basic Block 5 in flag array
				flags[i] = true;
			}
			
			for (i = 0; i <= SIZE; i++) {
				if (flags[i]) {
					//Insert free Basic Block 9 in flag array
					prime = i + i + 3;
					for (k = i + prime; k <= SIZE; k += prime){
						Boolean o = false;
						flags[k] = o;
					}
					count++;
				}
			}
			iterations++;
			elapsedTime = System.currentTimeMillis() - startTime;
			if (elapsedTime >= 10000){
				break;
			}
		}
		seconds = elapsedTime / 1000.0;
		score = (int) Math.round(iterations / seconds);
		results1 = iterations + " iterations in " + seconds + " seconds";
		if (count != 1899)
			//Insert free Basic Block 18->19->21 in flag
			results2 = "Error: count <> 1899";
		else
			results2 = "Sieve score = " + score;

		System.out.println(results1);
		System.out.println(results2);
	}

	public static void main(String[] args) {
		teste();
	}

}
