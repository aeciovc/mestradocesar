**** Blocks ****
Block 0:
[preds: ] [succs: 1 2 ]
id = 1000;
temp$0 = new Carro;
specialinvoke temp$0.<Carro: void <init>()>();
c = temp$0;
temp$1 = new Orcamento;
specialinvoke temp$1.<Orcamento: void <init>()>();
o = temp$1;
temp$2 = new OrcamentoService;
specialinvoke temp$2.<OrcamentoService: void <init>()>();
serviceOrcamento = temp$2;
temp$3 = new CarroService;
specialinvoke temp$3.<CarroService: void <init>()>();
carroService = temp$3;
placa = "";
temp$4 = virtualinvoke o.<Orcamento: int getId()>();
if id > temp$4 goto nop;

Block 1:
[preds: 0 ] [succs: 10 ]
goto [?= nop];

Block 2:
[preds: 0 ] [succs: 3 4 ]
nop;
temp$5 = virtualinvoke c.<Carro: java.lang.String getPlacaFromCarro()>();
bvf = temp$5;
if 11 == id goto nop;

Block 3:
[preds: 2 ] [succs: 5 ]
goto [?= nop];

Block 4:
[preds: 2 ] [succs: 9 ]
nop;
a = o;
temp$6 = <java.lang.System: java.io.PrintStream out>;
temp$7 = virtualinvoke a.<Orcamento: int getId()>();
virtualinvoke temp$6.<java.io.PrintStream: void println(int)>(temp$7);
goto [?= nop];

Block 5:
[preds: 3 ] [succs: 6 7 ]
nop;
temp$8 = virtualinvoke serviceOrcamento.<OrcamentoService: Orcamento buscarOrcamento(int)>(id);
o = temp$8;
temp$9 = virtualinvoke c.<Carro: int getId()>();
i = temp$9;
if id < 0 goto nop;

Block 6:
[preds: 5 ] [succs: 8 ]
goto [?= nop];

Block 7:
[preds: 5 ] [succs: 8 ]
nop;
temp$10 = <java.lang.System: java.io.PrintStream out>;
temp$11 = virtualinvoke o.<Orcamento: int getId()>();
virtualinvoke temp$10.<java.io.PrintStream: void println(int)>(temp$11);
temp$12 = i;
a = temp$12 + 9;
temp$13 = <java.lang.System: java.io.PrintStream out>;
virtualinvoke temp$13.<java.io.PrintStream: void println(int)>(a);

Block 8:
[preds: 6 7 ] [succs: 9 ]
nop;

Block 9:
[preds: 4 8 ] [succs: 11 ]
nop;
virtualinvoke carroService.<CarroService: void pintarCarro(java.lang.String)>(bvf);
temp$14 = new Carro;
specialinvoke temp$14.<Carro: void <init>()>();
c = temp$14;
goto [?= nop];

Block 10:
[preds: 1 ] [succs: 11 ]
nop;
temp$15 = "";
placa = temp$15;

Block 11:
[preds: 9 10 ] [succs: ]
nop;
virtualinvoke carroService.<CarroService: void pintarCarro(java.lang.String)>(placa);
temp$16 = 30;
return temp$16;