import java.util.Map;

import soot.Body;
import soot.BodyTransformer;
import soot.PackManager;
import soot.PhaseOptions;
import soot.Transform;
import soot.Unit;
import soot.options.Options;


public class japTransformer {

	public static void main(String[] args) {
		  PackManager.v().getPack("jap").add(
		      new Transform("jap.myTransform", new BodyTransformer() {

		        protected void internalTransform(Body body, String phase, Map options) {
		          for (Unit u : body.getUnits()) {
		            System.out.println(u.getTags());
		          }
		        }

		      }));
		  Options.v().set_verbose(true);
		  PhaseOptions.v().setPhaseOption("jap.npc", "on");
		  soot.Main.main(args);
		}

}
