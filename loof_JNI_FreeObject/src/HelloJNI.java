public class HelloJNI {
   
   static {
      System.loadLibrary("helloJNI"); // Load native library at runtime
                                   // hello.dll (Windows) or libhello.so (Unixes)
   }
 
   // Declare a native method sayHello() that receives nothing and returns void
   private native void sayHello();
   private native void setTag(Object o);
 
   // Test Driver
   public static void main(String[] args) {
      
      



      Object g = new Object();
      HelloJNI jni = new HelloJNI();
      //jni._setTag(g);
      jni.processSieve();
      
        // invoke the native method

      
   }

   public void processSieve(){

      String results1, results2;
      results1 = "Running Sieve benchmark.";
      results2 = "This will take about 10 seconds.";

      Integer SIZE = 8190;
      Boolean flags[] = new Boolean[SIZE + 1];
      Integer prime, k, iter, count;
      Integer iterations = 0;
      Double seconds = 0.0;
      Integer score = 0;
      Long startTime, elapsedTime;
      setTag(results1);
      sayHello();

      startTime = System.currentTimeMillis();
      while (true) {
         count = 0;
         for (Integer i = 0; i <= SIZE; i++){
            flags[i] = true;
            
         }
         
         for (Integer i = 0; i <= SIZE; i++) {
            if (flags[i]) {
               prime = i + i + 3;
               for (k = i + prime; k <= SIZE; k += prime){
                  flags[k] = false;
               }
               count++;
            }
         }
         iterations++;
         elapsedTime = System.currentTimeMillis() - startTime;
         //_setTag(elapsedTime);
         if (elapsedTime >= 10000){
            break;
         }
      }
      seconds = elapsedTime / 1000.0;
      //_setTag(seconds);
      score = (int) Math.round(iterations / seconds);
      results1 = iterations + " iterations in " + seconds + " seconds";
      if (count != 1899)
         results2 = "Error: count <> 1899";
      else
         results2 = "Sieve score = " + score;

      System.out.println(results1);
      System.out.println(results2);

   }
}
