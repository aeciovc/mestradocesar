package compile.time.deallocation;

import java.util.Random;

public class A {
	int[] a;

	void run(int n) {
		B x = null;
		while (true) {
			x = new B(this.a); //Aloca��o 1
			this.fill(n);
			x.compute(this.a);
		}
	}

	void fill(int n) {
		this.a = new int[n]; //Aloca��o 2
		while (n > 0)
			this.a[--n] = new Random().nextInt();
	}
}
