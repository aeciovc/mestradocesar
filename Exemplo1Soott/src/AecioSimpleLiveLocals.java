import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import princeton.AdjMatrixEdgeWeightedDigraph;
import princeton.DirectedEdge;
import princeton.FloydWarshall;
import princeton.StdOut;
import soot.Body;
import soot.G;
import soot.Local;
import soot.RefType;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Timers;
import soot.Type;
import soot.Unit;
import soot.ValueBox;
import soot.jimple.Jimple;
import soot.jimple.StringConstant;
import soot.jimple.internal.JAssignStmt;
import soot.jimple.internal.JIfStmt;
import soot.jimple.internal.JReturnStmt;
import soot.jimple.parser.node.Node;
import soot.options.Options;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.ExceptionalBlockGraph;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.toolkits.graph.UnitGraph;
import soot.toolkits.scalar.ArraySparseSet;
import soot.toolkits.scalar.BackwardFlowAnalysis;
import soot.toolkits.scalar.FlowSet;
import soot.toolkits.scalar.LiveLocals;

public class AecioSimpleLiveLocals implements LiveLocals {

	Map<Unit, List> unitToLocalsAfter;
	Map<Unit, List> unitToLocalsBefore;
	ArrayList<Variable> variables = new ArrayList<Variable>();
	UnitGraph graph;
	ExceptionalBlockGraph blockGraph;
	
	ArrayList<Block> blocksGetFromDtoU;
	Block initBlock;
	
	ArrayList<PathDefined> paths = new ArrayList<PathDefined>();
	
	//FlodyWarshal
	AdjMatrixEdgeWeightedDigraph flodyGraph;
	
	//SootTransformation
	SootClass sClass;
	
	Body bodyTransformation;
	
	public static final boolean DEBUG_ON = false;

	/**
	 * Computes the analysis given a UnitGraph computed from a method body. It
	 * is recommended that a ExceptionalUnitGraph (or similar) be provided for
	 * correct results in the case of exceptional control flow.
	 * 
	 * @param g
	 *            a graph on which to compute the analysis.
	 * 
	 * @see ExceptionalUnitGraph
	 */
	public AecioSimpleLiveLocals(UnitGraph graph, ExceptionalBlockGraph blockGraph, SootClass sClass) {
		if (Options.v().time())
			Timers.v().liveTimer.start();

		if (Options.v().verbose())
			G.v().out.println("[" + graph.getBody().getMethod().getName()
					+ "]     Constructing SimpleLiveLocals...");
		
		this.graph = graph;
		this.blockGraph = blockGraph;
		this.sClass = sClass;
		
		//Generate Initial Body
		bodyTransformation = blockGraph.getBody();
		
		SimpleLiveLocalsAnalysis analysis = new SimpleLiveLocalsAnalysis(graph);

		if (Options.v().time())
			Timers.v().livePostTimer.start();

		// Build unitToLocals map
		{
			unitToLocalsAfter = new HashMap<Unit, List>(graph.size() * 2 + 1,
					0.7f);
			unitToLocalsBefore = new HashMap<Unit, List>(graph.size() * 2 + 1,
					0.7f);

			Iterator unitIt = graph.iterator();

			while (unitIt.hasNext()) {
				Unit s = (Unit) unitIt.next();

				// Get variables avaible
				Iterator variableBox = s.getUseAndDefBoxes().iterator();

				while (variableBox.hasNext()) {
					ValueBox box = (ValueBox) variableBox.next();

					if (box.getValue() instanceof Local) {
						variables.add(new Variable(box.getValue().toString(), box.getValue().getType().toString()));
					}
				}

				FlowSet set = (FlowSet) analysis.getFlowBefore(s);
				unitToLocalsBefore.put(s,
						Collections.unmodifiableList(set.toList()));

				set = (FlowSet) analysis.getFlowAfter(s);
				unitToLocalsAfter.put(s,
						Collections.unmodifiableList(set.toList()));
			}
		}

		if (Options.v().time())
			Timers.v().livePostTimer.end();

		if (Options.v().time())
			Timers.v().liveTimer.end();
	}

	// My implementation

	public List getLiveLocalsAfter(Unit s) {
		s.getTags(); // list line numbers
		return unitToLocalsAfter.get(s);
	}

	public List getLiveLocalsBefore(Unit s) {
		return unitToLocalsBefore.get(s);
	}

	public List<Variable> getAllVariables() {
		return variables;
	}
	
	public List<Variable> getValidVariables() {
		
		ArrayList<Variable> validVariables = new ArrayList<Variable>();
		for (int i = 0; i < variables.size(); i++) {
			
			if (!variables.get(i).getName().contains("$") && !validVariables.contains(variables.get(i)) && !variables.get(i).isArray()){
				validVariables.add(variables.get(i));
			}
			
		}
		
		return validVariables;
	}

	public Unit getMaxNodeUseToVariable(String variableName) {

		Unit limitNode = null;
		ArrayList<Unit> nodesUse = new ArrayList<Unit>();
		
		if (DEBUG_ON)
			System.out
					.println("Nos: =======================================================>>>>>>>>");
		for (Unit u : graph) {

			Unit key = u;
	
			if (DEBUG_ON)
				System.out.println(key.toString());
			List<Local> before = getLiveLocalsBefore(u);
			List<Local> after = getLiveLocalsAfter(u);

			if (before != null && after != null){
				
				List<Local> newList = new ArrayList<Local>(before.size() + after.size());
				newList.addAll(before);
				newList.addAll(after);

				for (Local local : newList) {

					if (variableName.equals(local.getName())) {
						nodesUse.add(key);
						break;
					}
				}
				
			}
			
			
		}

		// Get Last Unit used
		if (nodesUse == null || nodesUse.size() == 0){
			return getNodeDeclarationToVariable(variableName);
		}
		
		limitNode = nodesUse.get(nodesUse.size() - 1);

		if (limitNode instanceof JReturnStmt) {
			return limitNode;
		} else if (limitNode instanceof JAssignStmt) {
			// Left
			ValueBox variableUse = limitNode.getDefBoxes().get(0);

			if (!variableUse.getValue().toString().equals(variableName)) {

				if (isPrimitiveType(variableUse.getValue().getType())) {
					return limitNode;
				} else {
					
					//if (limitNode.getDefBoxes().get(0).)
					
					if (isInvocationMethod(limitNode)){
						return limitNode;
					}else{
						return getMaxNodeUseToVariable(variableUse.getValue()
								.toString());
					}
						
				}

			} else {
				return limitNode;
			}

		} else if (limitNode instanceof JIfStmt) {
			return limitNode;
		} else {
			return limitNode;
		}

	}
	
	public boolean isInvocationMethod(Unit node){

		if (node instanceof JAssignStmt) {

			for (int i = 0; i < node.getUseBoxes().size(); i++) {

				ValueBox v = node.getUseBoxes().get(i);
				if (v.getValue().toString().contains("virtualinvoke")) {
					return true;
				}

			}
		}
		
		return false;
	
	}
	
	public Unit getNodeDeclarationToVariable(String variableName){
		
		for (Unit u : graph) {

			Unit key = u;
				
			if (key.toString().contains(variableName)){
				return key;
			}	
		}
		
		return null;
	}

	public boolean isPrimitiveType(Type type) { 
		return type.toString().equals("java.lang.String") || type.toString().equals("int")
				|| type.toString().equals("boolean") || type.toString().equals("double")
				|| type.toString().equals("float") || type.toString().equals("long");
	}

	public void free() {
		//
	}

	public String getVariablesGen(Unit u) {
		return "";
	}

	public String getVariablesKill(Unit u) {
		return "";
	}
	
	private ArrayList<String> pointsToDeallocation = new ArrayList<String>();
	
	public void generatePoints(Variable variable){
		
		Unit lastNode = getMaxNodeUseToVariable(variable.getName());
		System.out.println(" ");
		System.out.println("Last use to variable "+ variable.getName() + "-"+ variable.getType() + " is "+ lastNode.toString());
		
		initBlock = getBlockDeclared(variable.getName());
		
		Block lastBlock = getBlockFromNode(lastNode);
		
		
		//Não é possível determinar qual ultimo uso ou a variável não é usada. 
		//Caso de Exceções com variáveis de exception não usadas. é definido que o block declarado é o 0 (por conta do hoisting) mas se não é possível determinar não se faz uso
		if (lastBlock == null){
			return;
		}
		
		PathDefined pathDefined = new PathDefined(variable, initBlock, lastBlock);
		
		 // run Floyd-Warshall algorithm
        FloydWarshall spt = new FloydWarshall(flodyGraph);

        
        int blockNumberInit = initBlock.getIndexInMethod();
        int blockNumberLast = lastBlock.getIndexInMethod();
        
        //Adiciona o bloco de partida
        pathDefined.addBlockInOnPath(initBlock);
        
        //Verifica se existe caminho
        if (spt.hasPath(blockNumberInit, blockNumberLast)){
        	
        	StdOut.println("Path D: "+ blockNumberInit + " to U:"+ blockNumberLast+ " -> "+ spt.path(blockNumberInit, blockNumberLast));
        	
        	//Busca os caminhos
        	Iterator paths = spt.path(blockNumberInit, blockNumberLast).iterator();

			while (paths.hasNext()) {
				
				DirectedEdge d = (DirectedEdge) paths.next();
				
				Block b = getBlockFromNumber(d.to());
				pathDefined.addBlockInOnPath(b);
			}
        }
        
        pathDefined.processOutMarks();
        StdOut.println(pathDefined);
        
        markPointsOnCode(pathDefined);
       		
	}
        
    private void markPointsOnCode(PathDefined pathDefined) {
		
    	//Transformation
    	
		//Add System.out.print
    	String printVariable = "tmp"+ pathDefined.getVariable().getName() + new Random().nextInt(10000);
		Local tmpLocal = Jimple.v().newLocal(printVariable, RefType.v("java.io.PrintStream"));
		
		
		bodyTransformation.getLocals().add(tmpLocal);
		
		
		// add "tmpRef = java.lang.System.out"
		Unit tmpRefAssignUnit = Jimple.v().newAssignStmt(tmpLocal, Jimple.v().newStaticFieldRef(
										Scene.v()
										.getField("<java.lang.System: java.io.PrintStream out>").makeRef()));
		
		bodyTransformation.getUnits().addFirst(tmpRefAssignUnit);
		
		
		
		SootMethod toCall = Scene.v().getMethod
			      ("<java.io.PrintStream: void println(java.lang.String)>");
		
	
		
		/*body.getUnits().addFirst(Jimple.v().newInvokeStmt
		        (Jimple.v().newVirtualInvokeExpr
				           (tmpLocal, toCall.makeRef(), StringConstant.v("Hellos world!"))));*/
		
		String valuePrinted = formatStringPinted(pathDefined.getVariable());
		
		for (int i = 0; i < pathDefined.getBlocksOutOnPath().size(); i++) {
			
			Unit nodeToInsert = Jimple.v().newInvokeStmt
			        (Jimple.v().newVirtualInvokeExpr
					           (tmpLocal, toCall.makeRef(), StringConstant.v(valuePrinted)));
			
			Unit firstNode = pathDefined.getBlocksOutOnPath().get(i).getHead();
			
			bodyTransformation.getUnits().insertAfter(nodeToInsert, firstNode);
			
		}
			    
		bodyTransformation.validate();
	}
    
    public String formatStringPinted(Variable v){
    	
    	return "[LOOF] ["+v.getName()+"]"+"-["+v.getType()+"] ";
    	
    }
    public void insertFreeOnBlock(Block b){
    	
    	
    	
    	
    }

	public Block getBlockFromNumber(int number){
    	return blockGraph.getBlocks().get(number);
    }

	public void buildFlodyWarshall() {

		int countVertices = blockGraph.getBlocks().size();

		flodyGraph = new AdjMatrixEdgeWeightedDigraph(
				countVertices);

		for (int i = 0; i < countVertices; i++) {

			Block b = blockGraph.getBlocks().get(i);
			
			for (int j = 0; j < b.getSuccs().size(); j++){	
				flodyGraph.addEdge(new DirectedEdge(i, b.getSuccs().get(j).getIndexInMethod(), 1));
			}
	
		}
		//StdOut.println("...Flody Warshal output...");

		//StdOut.println(flodyGraph);

	}
	
	public Body getBodyTransformed(){
		return bodyTransformation;
	}
	
	public Block getBlockDeclared(String variableName){
		
		Block resultBlock = null;
		Block block0 = null;
		for (Block block : blockGraph.getBlocks()) {
		
			resultBlock = block;
			
			if (block0 == null){
				block0 = block;
			}
			
			Iterator variableBox = resultBlock.iterator();

			while (variableBox.hasNext()) {
				
				Unit u = (Unit) variableBox.next();

				if (u instanceof JAssignStmt) {
					ValueBox box = (ValueBox) u.getDefBoxes().iterator().next();

					if (box.getValue() instanceof Local) {
						if (box.getValue().toString().equals(variableName)) {
							return resultBlock;
						}
					}
				}
			}			
		}
		
		//new Exception("Basic Block not found in getBlockDeclared(String variableName)");		
		return block0;
		
		
	}
	
	public Block getBlockFromNode(Unit node){
		
		Block resultBlock = null;
		for (Block block : blockGraph.getBlocks()) {
		
			resultBlock = block;
			
			Iterator variableBox = resultBlock.iterator();

			while (variableBox.hasNext()) {
				
				Unit u = (Unit) variableBox.next();

				if (u.equals(node)){
					return resultBlock;
				}
			}			
		}
		
		new Exception("Basic Block not found in getBlockFromNode(Unit node)");		
		return null;
	}
	
	public String markPoint(Unit u){
		return u.toString();
	}
	
	public UnitGraph getGraph(){
		return graph;
	}
	
	public Unit getNodeBefore(Unit u){
		return graph.getPredsOf(u).get(0);
	}
	
	public Unit getNodeAfter(Unit u){
		return graph.getSuccsOf(u).get(0);
	}
	
}

class SimpleLiveLocalsAnalysis extends BackwardFlowAnalysis {
	FlowSet emptySet;
	Map<Unit, FlowSet> unitToGenerateSet;
	Map<Unit, FlowSet> unitToKillSet;

	SimpleLiveLocalsAnalysis(UnitGraph g) {
		super(g);

		if (Options.v().time())
			Timers.v().liveSetupTimer.start();

		emptySet = new ArraySparseSet();

		// Create kill sets.
		{
			unitToKillSet = new HashMap<Unit, FlowSet>(g.size() * 2 + 1, 0.7f);

			Iterator unitIt = g.iterator();
			while (unitIt.hasNext()) {
				Unit s = (Unit) unitIt.next();

				FlowSet killSet = emptySet.clone();

				Iterator boxIt = s.getDefBoxes().iterator();

				while (boxIt.hasNext()) {
					ValueBox box = (ValueBox) boxIt.next();

					if (box.getValue() instanceof Local)
						killSet.add(box.getValue(), killSet);
				}

				unitToKillSet.put(s, killSet);
			}
		}

		// Create generate sets
		{
			unitToGenerateSet = new HashMap<Unit, FlowSet>(g.size() * 2 + 1,
					0.7f);

			Iterator unitIt = g.iterator();

			while (unitIt.hasNext()) {
				Unit s = (Unit) unitIt.next();

				FlowSet genSet = emptySet.clone();

				Iterator boxIt = s.getUseBoxes().iterator();

				while (boxIt.hasNext()) {
					ValueBox box = (ValueBox) boxIt.next();

					if (box.getValue() instanceof Local)
						genSet.add(box.getValue(), genSet);
				}

				unitToGenerateSet.put(s, genSet);
			}
		}

		if (Options.v().time())
			Timers.v().liveSetupTimer.end();

		if (Options.v().time())
			Timers.v().liveAnalysisTimer.start();

		doAnalysis();

		if (Options.v().time())
			Timers.v().liveAnalysisTimer.end();

	}

	protected Object newInitialFlow() {
		return emptySet.clone();
	}

	protected Object entryInitialFlow() {
		return emptySet.clone();
	}

	protected void flowThrough(Object inValue, Object unit, Object outValue) {
		FlowSet in = (FlowSet) inValue, out = (FlowSet) outValue;

		// Perform kill
		in.difference(unitToKillSet.get(unit), out);

		// Perform generation
		out.union(unitToGenerateSet.get(unit), out);
	}

	protected void merge(Object in1, Object in2, Object out) {
		FlowSet inSet1 = (FlowSet) in1, inSet2 = (FlowSet) in2;

		FlowSet outSet = (FlowSet) out;

		inSet1.union(inSet2, outSet);
	}

	protected void copy(Object source, Object dest) {
		FlowSet sourceSet = (FlowSet) source, destSet = (FlowSet) dest;

		sourceSet.copy(destSet);
	}
}
