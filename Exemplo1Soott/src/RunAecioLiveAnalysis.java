import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import soot.Body;
import soot.NormalUnitPrinter;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.UnitPrinter;
import soot.jimple.toolkits.annotation.logic.Loop;
import soot.options.Options;
import soot.toolkits.graph.Block;
import soot.toolkits.graph.BlockGraph;
import soot.toolkits.graph.DominatorNode;
import soot.toolkits.graph.DominatorTree;
import soot.toolkits.graph.ExceptionalBlockGraph;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.toolkits.graph.LoopNestTree;
import soot.toolkits.graph.UnitGraph;

public class RunAecioLiveAnalysis {
	
	public static final boolean DEBUG_ON = true;
	public static String file_analysis;
	
	public static void main(String[] args) {
		//args = new String[] { "TesteObject" };
		args = new String[] { "SieveMain" };
		if (args.length == 0) {
			System.out.println("Usage: java RunLiveAnalysis class_to_analyse");
			System.exit(0);
		}

		file_analysis = args[0];
		// IFDSReachingDefinitions

		String sep = File.separator;
		String pathSep = File.pathSeparator;
		String path = System.getProperty("java.home") + sep + "lib" + sep
				+ "rt.jar";
		path += pathSep + "." + sep + "tutorial" + sep + "guide" + sep
				+ "examples" + sep + "analysis_framework" + sep + "src";

		path += pathSep
				+ "/home/aecio/Workspaces/WorkspaceMestrado/mestradocesar_git/mestradocesar/Exemplo1Soott/src";
		
		String libFolder = "/home/aecio/Workspaces/WorkspaceMestrado/mestradocesar_git/mestradocesar/Exemplo1Soott/lib";
		path += pathSep + libFolder + "/" + "xalan.jar";
		path += pathSep + libFolder + "/" + "serializer.jar";
		path += pathSep + libFolder + "/" + "xercesImpl.jar";
		path += pathSep + libFolder + "/" + "xml-apis.jar";
		path += pathSep + libFolder + "/" + "xsltc.jar";
		
		Options.v().set_soot_classpath(path);

		SootClass sClass = Scene.v().loadClassAndSupport(args[0]);
		sClass.setApplicationClass();
		Scene.v().loadNecessaryClasses();
		Scene.v().addBasicClass("java.io.PrintStream",SootClass.SIGNATURES);
		Scene.v().addBasicClass("java.lang.System",SootClass.SIGNATURES);
		//Scene.v().loadClassAndSupport("java.lang.System");
		
		//CallGraph cg = Scene.v().getCallGraph();
		//IFDSReachingDefinitio
		
		for (SootMethod m : sClass.getMethods()) {
			Body b = m.retrieveActiveBody();
			showLoops(b);

			if (m.getName().equals("<init>") || m.getName().equals("main"))
				continue;

			if (DEBUG_ON){
				System.out.println("=======================================");
				System.out.println(m.getName());
			}

			//Avaliar criar o Graph via ExceptionalBlockGraph
			UnitGraph graph = new ExceptionalUnitGraph(b);
			
			
			ExceptionalBlockGraph graphBlock = new ExceptionalBlockGraph(b);
			
			//DirectedGraph r = new DirectedCallGraph(cg, filter, heads, verbose)
//			TrapUnitGraph r = new TrapUnitGraph(b);
//			ZonedBlockGraph f = new ZonedBlockGraph(b);
//			ClassicCompleteUnitGraph a = new ClassicCompleteUnitGraph(b);
//			BriefUnitGraph t = new BriefUnitGraph(b);
//			
//			showLoops(b);
//			MHGDominatorTree mh = new MHGDominatorTree(new MHGDominatorsFinder(g));
//			
//			String result = dominatorTreeToString(mh, mh.getHead());
//			System.out.println(result);
			
			AecioSimpleLiveLocals sll = new AecioSimpleLiveLocals(graph, graphBlock, sClass);

			sll.buildFlodyWarshall();
			
			System.out.println("Analisando variáveis: ");
			
			for (int i = 0; i < sll.getValidVariables().size(); i++) {
				System.out.print( sll.getValidVariables().get(i).getName() + " , ");
			}
			
			for (int i = 0; i < sll.getValidVariables().size(); i++) {
				Variable v = sll.getValidVariables().get(i);
				sll.generatePoints(v);
			}

			Body bodyResult = sll.getBodyTransformed();
			System.out.println("Result code" + bodyResult.toString());
			
			saveJimpleOfResults(bodyResult.toString());

			UnitPrinter printer = new NormalUnitPrinter(b);
			printer.setIndent("");

			
			//limitNode.toString(printer);
			System.out.println(printer.output());

/*			if (DEBUG_ON) {
				for (Unit u : graph) {
					List<Local> before = sll.getLiveLocalsBefore(u);
					List<Local> after = sll.getLiveLocalsAfter(u);
					UnitPrinter up = new NormalUnitPrinter(b);
					up.setIndent("");

					System.out
							.println("---------------------------------------");
					u.toString(up);
					System.out.println(up.output());
					System.out.print("Live in: {");
					sep = "";
					for (Local l : before) {
						System.out.print(sep);
						System.out.print(l.getName() + ": " + l.getType());
						sep = ", ";
					}
					System.out.println("}");
					System.out.print("Live out: {");
					sep = "";
					for (Local l : after) {
						System.out.print(sep);
						System.out.print(l.getName() + ": " + l.getType());
						sep = ", ";
					}
					System.out.println("}");
					System.out
							.println("---------------------------------------");
				}
				System.out.println("=======================================");

			}*/
		}
	}
	
	public static void saveJimpleOfResults(String content){
		PrintWriter writer = null;
		System.out.println("Saving file... ");
		String local = "/home/aecio/Workspaces/WorkspaceMestrado/mestradocesar_git/mestradocesar/Exemplo1Soott/src";
		
		try {
			writer = new PrintWriter(local+ "/jimple"+file_analysis+".txt", "UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		writer.println(content);
		System.out.println(" file saved... "+local+ "/jimple"+file_analysis+".txt");
		writer.close();
	}
	
	
	public static void showLoops(Body body){
		
        System.out.println("**** Body ****");
        System.out.println(body);
        System.out.println();

        System.out.println("**** Blocks ****");
        BlockGraph blockGraph = new ExceptionalBlockGraph(body);
        for (Block block : blockGraph.getBlocks()) {
        	System.out.println("Block : "+ block.getIndexInMethod());
        	
        	System.out.println(block);
           
        }
        System.out.println();

        System.out.println("**** Loops ****");
        
        
        
        LoopNestTree loopNestTree = new LoopNestTree(body);
        for (Loop loop : loopNestTree) {
            System.out.println("Found a loop with head: " + loop.getHead());
        }
		
	}
	
	 private static String dominatorTreeToString(DominatorTree dom, DominatorNode root)
	  {
	    String s = new String();
	    s += "\n Begin " + ((Block)root.getGode()).toShortString() + " ( ";
	    List children = dom.getChildrenOf(root);
	   
	    for(int i = 0; i < children.size(); i++)
	    {
	      s += dominatorTreeToString(dom, (DominatorNode) children.get(i));
	   
	    }
	    s += " ) end of " + ((Block)root.getGode()).toShortString();
	   
	    return s;
	   
	   
	  }
	
}
