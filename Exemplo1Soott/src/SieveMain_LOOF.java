
public class SieveMain_LOOF
{

	static boolean result1Deallocated = false;
	static int countDealocationsLOOF = 0;
	
    static void teste()
    {

    	
		String results1, results2;
		results1 = "Running Sieve benchmark.";
		results2 = "This will take about 10 seconds.";

		Integer SIZE = 8190;
		Boolean flags[] = new Boolean[SIZE + 1];
		Integer prime, k, iter, count;
		int i = 0;
		Integer iterations = 0;
		Double seconds = 0.0;
		Integer score = 0;
		Long startTime, elapsedTime;
		startTime = System.currentTimeMillis();
		
		while (true) {
			count = 0;
			for (i = 0; i <= SIZE; i++){//BasicBlock 3
				flags[i] = true;
				//countLOOFDealocations("[LOOF] i-Integer (BB 5)"); @@
				//Block inserted
				if (!result1Deallocated){
					countLOOFDealocations("[LOOF] result1-String (BB 5)");
					countLOOFDealocations("[LOOF] result2-String (BB 5)");
					countLOOFDealocations("[LOOF] iterations-Integer (BB 5)");
					countLOOFDealocations("[LOOF] seconds-Integer (BB 5)");
					countLOOFDealocations("[LOOF] score-Integer (BB 5)");
					countLOOFDealocations("[LOOF] startTime-Integer (BB 5)");
					countLOOFDealocations("[LOOF] count-Integer (BB 5)");
					
					result1Deallocated = true;
				}
			}
			
			for (i = 0; i <= SIZE; i++) {//BasicBlock 7
				//System.out.println("[LOOF] result1-String (BB 9)");//Não deve ser inserido
				if (flags[i]) { //BasicBlock 9
					
					prime = i + i + 3;
					//countLOOFDealocations("[LOOF] i-Integer (BB 10)");//BB 10
					for (k = i + prime; k <= SIZE; k += prime){
						flags[k] = false;
					}
					//BB13
					countLOOFDealocations("[LOOF] prime-Integer (BB 13)");
					countLOOFDealocations("[LOOF] k-Integer (BB 13)");
					count++;
				}
			}
			iterations++;
			elapsedTime = System.currentTimeMillis() - startTime;
			if (elapsedTime >= 10000){
				break;
			}
			//BB 20
			//System.out.println("[LOOF] result1-String (BB 20)");//Não deve ser inserido
			//System.out.println("[LOOF] iterations-Integer (BB 20)");//Não deve ser inserido
			if (!result1Deallocated){
				countLOOFDealocations("[LOOF] score-Integer (BB 20)");
			}
			//countLOOFDealocations("[LOOF] count-Integer (BB 20)");
			countLOOFDealocations("[LOOF] elapsedTime-Integer (BB 20)");//Não deve ser inserido /erro pois ele volta
		}
		
		seconds = elapsedTime / 1000.0;
		score = (int) Math.round(iterations / seconds);
		results1 = iterations + " iterations in " + seconds + " seconds";
		
		//BB 21
		countLOOFDealocations("[LOOF] SIZE-String (BB 21)");
		countLOOFDealocations("[LOOF] startTime-Integer (BB 21)");
		
		if (count != 1899){
			results2 = "Error: count <> 1899";
			countLOOFDealocations("[LOOF] iterations-Integer (BB 23)");
			countLOOFDealocations("[LOOF] seconds-Integer (BB 23)");
			countLOOFDealocations("[LOOF] score-Integer (BB 23)");
			countLOOFDealocations("[LOOF] count-Integer (BB 23)");
			countLOOFDealocations("[LOOF] elapsedTime-Integer (BB 23)");
		}else{
			
			results2 = "Sieve score = " + score;
			countLOOFDealocations("[LOOF] iterations-Integer (BB 22)");
			countLOOFDealocations("[LOOF] seconds-Integer (BB 22)");
			countLOOFDealocations("[LOOF] count-Integer (BB 22)");
			countLOOFDealocations("[LOOF] elapsedTime-Integer (BB 22)");
		}
		
		countLOOFDealocations("[LOOF] score-Integer (BB 25)");
		
		System.out.println(results1);
		System.out.println(results2);

      
    }

    public static void main(String[]  r0)
    {
        SieveMain_LOOF.teste();
        System.out.println("Quantidade de desalocações:"+ countDealocationsLOOF);
    }
    
    public static void countLOOFDealocations(String text){
    	//System.out.println(text);a
    	
    	countDealocationsLOOF = countDealocationsLOOF + 1;
    }
    
    
}
