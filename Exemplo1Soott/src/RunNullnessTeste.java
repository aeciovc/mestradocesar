import java.io.File;

import soot.Scene;
import soot.SootClass;
import soot.options.Options;


public class RunNullnessTeste {

	
	
	public static void main(String[] args) {
		
		args = new String[] {"NullnessTeste"};
		
		if (args.length == 0) {
			System.out.println("Usage: java RunLiveAnalysis class_to_analyse");
			System.exit(0);
		}
		
		//IFDSReachingDefinitions

		String sep = File.separator;
		String pathSep = File.pathSeparator;
		String path = System.getProperty("java.home") + sep + "lib" + sep
				+ "rt.jar";
		path += pathSep + "." + sep + "tutorial" + sep + "guide" + sep
				+ "examples" + sep + "analysis_framework" + sep + "src";
		
		path += pathSep + "/Users/aeciocosta/Desktop/Mestrado/Soot/Apps/Exemplo1Soott/src";
		Options.v().set_soot_classpath(path);

		
		SootClass sClass = Scene.v().loadClassAndSupport(args[0]);
		sClass.setApplicationClass();
		Scene.v().loadNecessaryClasses();
		
		NullnessTeste na=new NullnessTeste(b);
		// a SparseArraySet of non-null variables.
		na.getFlowBefore(s);
		// another SparseArraySet
		if (s.fallsThrough()) na.getFallFlowAfter(s);
		// a List of SparseArraySets
		if (s.branches()) na.getBranchFlowAfter(s);
		
		
	}
	
	

}
