
import java.io.FileWriter;
import java.io.PrintWriter;
import org.apache.xalan.trace.PrintTraceListener;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamSource;
import org.apache.xalan.transformer.TransformerImpl;
import org.apache.xalan.trace.TraceManager;
import javax.xml.transform.stream.StreamResult;
import java.io.PrintStream;
import java.io.IOException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;
import java.util.TooManyListenersException;
import org.xml.sax.SAXException;

public class Bench_Xalan_Trace_LOOF {

	public static void teste() throws java.io.IOException,
			javax.xml.transform.TransformerException,
			javax.xml.transform.TransformerConfigurationException,
			java.util.TooManyListenersException, org.xml.sax.SAXException {
 
		String r0, r1, r2, r3, r4;
		FileWriter r6, r7;
		PrintWriter r9;
		PrintTraceListener r11;
		boolean z5;
		TransformerFactory r13;
		Transformer r15;
		StreamSource r16, r17;
		StringBuffer r18, r19, r20, r21;
		TransformerImpl r22;
		TraceManager r24;
		StreamResult r25;
		PrintStream r26, r27, r28, r29, r30, r31, r32, r33, r34;
		r34 = System.out;
		r33 = System.out;
		r32 = System.out;
		r31 = System.out;
		r30 = System.out;
		r29 = System.out;
		r28 = System.out;
		r27 = System.out;
		r0 = "bench_files/foo";
		r6 = new FileWriter("events.log");
		r9 = new PrintWriter(r6, true);
		r11 = new PrintTraceListener(r9);
		r11.m_traceElements = true;
		r11.m_traceGeneration = true;
		r11.m_traceSelection = true;
		r11.m_traceTemplates = true;
		r11.m_traceExtension = true;
		r13 = TransformerFactory.newInstance();
		r18 = new StringBuffer();
		r18.append(r0);
		r18.append(".xsl");
		r1 = r18.toString();
		r16 = new StreamSource(r1);
		r15 = r13.newTransformer(r16);
		z5 = r15 instanceof TransformerImpl;

		if (z5) {
			r22 = (TransformerImpl) r15;
			r24 = r22.getTraceManager();
			r24.addTraceListener(r11);
			r19 = new StringBuffer();
			r19.append(r0);
			r19.append(".xml");
			r2 = r19.toString();
			r17 = new StreamSource(r2);
			r20 = new StringBuffer();
			r20.append(r0);
			r20.append(".out");
			r3 = r20.toString();
			r7 = new FileWriter(r3);
			r25 = new StreamResult(r7);
			r15.transform(r17, r25);
		}

		r34.println("[LOOF] [trMgr]-[org.apache.xalan.trace.TraceManager] ");
		r33.println("[LOOF] [transformerImpl]-[org.apache.xalan.transformer.TransformerImpl] ");
		r32.println("[LOOF] [transformer]-[javax.xml.transform.Transformer] ");
		r31.println("[LOOF] [tFactory]-[javax.xml.transform.TransformerFactory] ");
		r30.println("[LOOF] [ptl]-[org.apache.xalan.trace.PrintTraceListener] ");
		r9.close();
		r6.close();
		r26 = System.out;
		r21 = new StringBuffer();
		r21.append("**The output is in ");
		r21.append(r0);
		r21.append(".out; the log is in events.log ****");
		r4 = r21.toString();
		r26.println(r4);
	}

	public static void main(String[] args) throws java.io.IOException,
			TransformerException, TransformerConfigurationException,
			java.util.TooManyListenersException, org.xml.sax.SAXException {

		teste();

	}
}
