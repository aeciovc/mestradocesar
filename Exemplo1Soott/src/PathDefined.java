import java.util.ArrayList;
import java.util.List;

import soot.toolkits.graph.Block;


public class PathDefined {
	
	private Variable variable;

	private Block init;
	private Block last;
	private ArrayList<Block> blocksInOnPath = new ArrayList<Block>();
	private ArrayList<Block> blocksOutOnPath = new ArrayList<Block>();

	public PathDefined(Variable variable, Block init, Block last) {
		super();
		this.variable = variable;
		this.init = init;
		this.last = last;
	}
	public Block getInit() {
		return init;
	}
	public void setInit(Block init) {
		this.init = init;
	}
	public Block getLast() {
		return last;
	}
	public void setLast(Block last) {
		this.last = last;
	}
	public ArrayList<Block> getBlocksInOnPath() {
		return blocksInOnPath;
	}

	public ArrayList<Block> getBlocksOutOnPath() {
		return blocksOutOnPath;
	}
	
	public void addBlockOutOnPath(Block b) {
		this.blocksOutOnPath.add(b);
	}
	
	public void addBlockInOnPath(Block b) {
		this.blocksInOnPath.add(b);
	}
	
	public void processOutMarks(){
		
		for (int i = 0; i < blocksInOnPath.size(); i++) {

			List<Block> blocksSucc = blocksInOnPath.get(i).getSuccs();

			if (blocksSucc != null && blocksSucc.size() > 0) {
				for (int j = 0; j < blocksSucc.size(); j++) {
					Block sucessBlock = blocksSucc.get(j);
					if (!isExistBlock(sucessBlock, blocksOutOnPath) && !isExistBlock(sucessBlock, blocksInOnPath)) {
						addBlockOutOnPath(sucessBlock);
					}
				}
			}
		}
		
	}
	
	private boolean isExistBlock(Block b, ArrayList<Block> blocks) {
		boolean isExist = false;
		for (int i = 0; i < blocks.size(); i++) {

			if (blocks.get(i).equals(b)){
				isExist = true;
				break;
			}
			
		}
		
		return isExist;
	}
	@Override
	public String toString(){
		
		String print = "From "+ init.getIndexInMethod() + " to "+last.getIndexInMethod() + "\n Blocks In: ";
		
		String blocksIn = " ";
		for (int i = 0; i < blocksInOnPath.size(); i++){
			blocksIn = blocksIn + " - " +blocksInOnPath.get(i).getIndexInMethod();
		}
		
		String blocksOut = " \n Blocks Out:  ";
		for (int i = 0; i < blocksOutOnPath.size(); i++){
			blocksOut = blocksOut + " - " +blocksOutOnPath.get(i).getIndexInMethod();
		}
		
		return print + blocksIn + blocksOut;
		
	}
	public Variable getVariable() {
		return variable;
	}
	public void setVariable(Variable variable) {
		this.variable = variable;
	}


}
