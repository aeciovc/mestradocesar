import soot.Body;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Unit;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.toolkits.graph.UnitGraph;
import soot.toolkits.scalar.FlowSet;
import soot.toolkits.scalar.ForwardFlowAnalysis;

public class OtherMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*
		 * PackManager.v().getPack("jtp") .add(new Transform("jtp.myTransform",
		 * new BodyTransformer() {
		 * 
		 * protected void internalTransform(Body body, String phase, Map
		 * options) { new MyAnalysis(new ExceptionalUnitGraph(body)); // use
		 * G.v().out instead of System.out so that Soot can // redirect this
		 * output to the Eclipse console G.v().out.println(body.getMethod());
		 * G.v().out.println("Phase:" + phase); }
		 * 
		 * }));
		 * 
		 * soot.Main.main(args);
		 */

		SootClass c = Scene.v().loadClassAndSupport("Teste");
		c.setApplicationClass();
		
		SootMethod m = c.getMethodByName("main");
		
		Body b = m.retrieveActiveBody();
		
		UnitGraph g = new ExceptionalUnitGraph(b);
		
		MyFwdAnalysis an = new MyFwdAnalysis(g);
		for (Unit unit : g) {
			FlowSet in = an.getFlowBefore(unit);
			FlowSet out = an.getFlowAfter(unit);
		}

	}

	public static class MyFwdAnalysis extends ForwardFlowAnalysis<Unit, FlowSet> {

		public MyFwdAnalysis(UnitGraph graph) {
			super(graph);
			doAnalysis();
		}

		protected FlowSet entryInitialFlow() {
			return new FlowSet();
		}

		protected FlowSet newInitialFlow() {
			return new FlowSet();
		}

		protected void merge(FlowSet inSet1, FlowSet inSet2, FlowSet outSet) {
			inSet1.intersection(inSet2, outSet);
		}

		protected void copy(FlowSet srcSet, FlowSet dstSet) {
			srcSet.copy(dstSet);
		}

		protected void flowThrough(FlowSet inSet, Unit node, FlowSet outSet) {
			//Kill(inSet, node, outSet);
			//Gen(outSet, node);
		}
	}
}
