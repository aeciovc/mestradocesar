
#include "jni.h"
#include "stdio.h"
#include "HelloJNI.h"
#include "stdlib.h"
#include "jvmti.h"

#include <string.h>
#include <stddef.h>
#include <stdarg.h>
#include <time.h>
 

typedef struct {
    /* JVMTI Environment */
    jvmtiEnv      *jvmti;
} GlobalAgentData;

static GlobalAgentData *gdata;
//###########################
static int totalDeallocated = 0;


// Implementation of native method sayHello() of HelloJNI class
JNIEXPORT void JNICALL Java_HelloJNI_sayHello(JNIEnv *env, jobject thisObj) {
   printf("Hello World! hey\n");
   return;
}

// Implementation of native method sayHello() of HelloJNI class
JNIEXPORT void JNICALL Java_HelloJNI_setTag(JNIEnv *env, jobject thisObj, jobject other) {
   //printf("tagging object\n ");
	jlong tag;
   
   	int r = rand0();
	printf("tagging JNI: %d,",r);
	tag = r;

   jvmtiError err = (*gdata->jvmti)->SetTag(gdata->jvmti, other, tag);	
   return;
}



//00000000000000000000000000000000000000000000000000000000000000000000000000000000000000
/**
 * Callback for when an object is freed - we need to delete our tag on the object too.
 * Note that because our tag is just malloc'ed (and not a java object), we can trivially free
 * it directly within this callback.
 */

 /* Send message to stderr or whatever the error output location is and exit  */
void
fatal_error(const char * format, ...)
{
    va_list ap;

    va_start(ap, format);
    (void)vfprintf(stderr, format, ap);
    (void)fflush(stderr);
    va_end(ap);
    exit(3);
}

void check_jvmti_error(jvmtiEnv *jvmti, jvmtiError errnum, const char *str)
{
    if ( errnum != JVMTI_ERROR_NONE ) {
        char       *errnum_str;

        errnum_str = NULL;
        (void)(*jvmti)->GetErrorName(jvmti, errnum, &errnum_str);

        fatal_error("ERROR: JVMTI: %d(%s): %s\n", errnum,
                (errnum_str==NULL?"Unknown":errnum_str),
                (str==NULL?"":str));
    }
}


static void JNICALL cbObjectFree(jvmtiEnv *jvmti_env, jlong tag) {
	
	//printf("Free Object... %ld \n", tag);
	//fflush(stdout);
	totalDeallocated = totalDeallocated +1;

}

static unsigned long int next = 1;
int rand0(void){

     next = next * 1103515245 + 12345;
     return (unsigned int) (next/65536) % 32768;
}

static void JNICALL cbVMObjectAlloc(jvmtiEnv *jvmti, JNIEnv *env, jthread thread, 
		jobject object, jclass object_klass, jlong size)
{
            
    jlong tag;
   
   	int r = rand0();
	//printf("Allocate and Tagging: %d \n",r);
	tag = r;

    jvmtiError err = (*jvmti)->SetTag(jvmti, object, tag);

}

/* Callback for JVMTI_EVENT_VM_START */
static void JNICALL cbVMStart(jvmtiEnv *jvmti, JNIEnv *env)
{
    //enterCriticalSection(jvmti); {
 
 		printf("start ...\n");
    
    	/* Indicate VM has started */
    	//gdata->vmStarted = JNI_TRUE;

    //} exitCriticalSection(jvmti);
}

/* Callback for JVMTI_EVENT_VM_INIT */
static void JNICALL cbVMInit(jvmtiEnv *jvmti, JNIEnv *env, jthread thread)
{
    printf("cbVMInit ...\n");
}

/* Callback for JVMTI_EVENT_VM_DEATH */
static void JNICALL cbVMDeath(jvmtiEnv *jvmti, JNIEnv *env)
{
 	printf("cbVMDeath ...\n");
 	printf("Total de desalocacoes: ...%d\n",totalDeallocated);

}

JNIEXPORT jint JNICALL Agent_OnLoad(JavaVM *vm, char *options, void *reserved)
{
	static GlobalAgentData data;
    jvmtiEnv              *jvmti;
    jvmtiError             error;
    jint                   res;

    jvmtiCapabilities      capabilities;
    jvmtiEventCallbacks    callbacks;
    printf("0-everything ok!...\n");
    /* We return JNI_OK to signify success */

    (void)memset((void*)&data, 0, sizeof(data));
     gdata = &data;

    /* First thing we need to do is get the jvmtiEnv* or JVMTI environment */
    res = (*vm)->GetEnv(vm, (void **)&jvmti, JVMTI_VERSION_1);
    if (res != JNI_OK) {
        /* This means that the VM was unable to obtain this version of the
         *   JVMTI interface, this is a fatal error.
         */
        fatal_error("ERROR: Unable to access JVMTI Version 1 (0x%x),"
                " is your JDK a 5.0 or newer version?"
                " JNIEnv's GetEnv() returned %d\n",
               JVMTI_VERSION_1, res);
    }

    /* Here we save the jvmtiEnv* for Agent_OnUnload(). */
    gdata->jvmti = jvmti;

    /* Parse any options supplied on java command line */
    //parse_agent_options(options);

    /* Immediately after getting the jvmtiEnv* we need to ask for the
     *   capabilities this agent will need.
     */
    (void)memset(&capabilities,0, sizeof(capabilities));
    capabilities.can_generate_all_class_hook_events = 1;
    capabilities.can_tag_objects  = 1;
    capabilities.can_generate_object_free_events  = 1;
    capabilities.can_get_source_file_name  = 1;
    capabilities.can_get_line_numbers  = 1;
    capabilities.can_generate_vm_object_alloc_events  = 1;
    error = (*jvmti)->AddCapabilities(jvmti, &capabilities);
    check_jvmti_error(jvmti, error, "Unable to get necessary JVMTI capabilities.");

    /* Next we need to provide the pointers to the callback functions to
     *   to this jvmtiEnv*
     */
    (void)memset(&callbacks,0, sizeof(callbacks));
    /* JVMTI_EVENT_VM_START */
    callbacks.VMStart           = &cbVMStart;
    /* JVMTI_EVENT_VM_INIT */
    callbacks.VMInit            = &cbVMInit;
    /* JVMTI_EVENT_VM_DEATH */
    callbacks.VMDeath           = &cbVMDeath;
    /* JVMTI_EVENT_OBJECT_FREE */
    callbacks.ObjectFree        = &cbObjectFree;
    /* JVMTI_EVENT_VM_OBJECT_ALLOC */
    callbacks.VMObjectAlloc     = &cbVMObjectAlloc;
    /* JVMTI_EVENT_CLASS_FILE_LOAD_HOOK */
    //callbacks.ClassFileLoadHook = &cbClassFileLoadHook;
    
    error = (*jvmti)->SetEventCallbacks(jvmti, &callbacks, (jint)sizeof(callbacks));
    check_jvmti_error(jvmti, error, "Cannot set jvmti callbacks");

    /* At first the only initial events we are interested in are VM
     *   initialization, VM death, and Class File Loads.
     *   Once the VM is initialized we will request more events.
     */
    error = (*jvmti)->SetEventNotificationMode(jvmti, JVMTI_ENABLE,
                          JVMTI_EVENT_VM_START, (jthread)NULL);
    check_jvmti_error(jvmti, error, "Cannot set event notification");
    error = (*jvmti)->SetEventNotificationMode(jvmti, JVMTI_ENABLE,
                          JVMTI_EVENT_VM_INIT, (jthread)NULL);
    check_jvmti_error(jvmti, error, "Cannot set event notification");
    error = (*jvmti)->SetEventNotificationMode(jvmti, JVMTI_ENABLE,
                          JVMTI_EVENT_VM_DEATH, (jthread)NULL);
    check_jvmti_error(jvmti, error, "Cannot set event notification");
    error = (*jvmti)->SetEventNotificationMode(jvmti, JVMTI_ENABLE,
                          JVMTI_EVENT_OBJECT_FREE, (jthread)NULL);
    check_jvmti_error(jvmti, error, "Cannot set event notification");
    error = (*jvmti)->SetEventNotificationMode(jvmti, JVMTI_ENABLE,
                          JVMTI_EVENT_VM_OBJECT_ALLOC, (jthread)NULL);
    check_jvmti_error(jvmti, error, "Cannot set event notification");
    error = (*jvmti)->SetEventNotificationMode(jvmti, JVMTI_ENABLE,
                          JVMTI_EVENT_CLASS_FILE_LOAD_HOOK, (jthread)NULL);
    check_jvmti_error(jvmti, error, "Cannot set event notification");

    /* Here we create a raw monitor for our use in this agent to
     *   protect critical sections of code.
     */
    //error = (*jvmti)->CreateRawMonitor(jvmti, "agent data", &(gdata->lock));
    //check_jvmti_error(jvmti, error, "Cannot create raw monitor");

    /* Add jar file to boot classpath */
    //add_demo_jar_to_bootclasspath(jvmti, "heapTracker");

    printf("0-everything ok!...\n");
    /* We return JNI_OK to signify success */
    return JNI_OK;
}

/* Agent_OnUnload: This is called immediately before the shared library is
 *   unloaded. This is the last code executed.
 */
JNIEXPORT void JNICALL Agent_OnUnload(JavaVM *vm)
{
    /* Skip any cleanup, VM is about to die anyway */
}



