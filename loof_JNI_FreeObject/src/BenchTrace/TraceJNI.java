

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.xalan.trace.PrintTraceListener;
import org.apache.xalan.trace.TraceManager;
import org.apache.xalan.transformer.TransformerImpl;

/**
 * Sample for demonstrating Xalan "trace" interface. Usage: run in Trace
 * directory: java Trace For an extensions trace sample, run in extensions
 * directory: java Trace 3-java-namespace
 */
public class TraceJNI {


 	static {
      System.loadLibrary("traceJNI"); // Load native library at runtime
                                   // hello.dll (Windows) or libhello.so (Unixes)
   	}

   	  // Declare a native method sayHello() that receives nothing and returns void
   	private static native void sayHello();
   	private static native void setTag(Object o);


	public static void teste() throws java.io.IOException,
			TransformerException, TransformerConfigurationException,
			java.util.TooManyListenersException, org.xml.sax.SAXException {

		String fileName = "bench_files/foo";
		setTag(fileName);
		// Set up a PrintTraceListener object to print to a file.
		java.io.FileWriter fw = new java.io.FileWriter("events.log");
		setTag(fw);
		java.io.PrintWriter pw = new java.io.PrintWriter(fw, true);
		setTag(pw);
		PrintTraceListener ptl = new PrintTraceListener(pw);
		setTag(ptl);

		// Print information as each node is 'executed' in the stylesheet.
		ptl.m_traceElements = true;
		// Print information after each result-tree generation event.
		ptl.m_traceGeneration = true;
		// Print information after each selection event.
		ptl.m_traceSelection = true;
		// Print information whenever a template is invoked.
		ptl.m_traceTemplates = true;
		// Print information whenever an extension call is made.
		ptl.m_traceExtension = true;

		// Set up the transformation
		TransformerFactory tFactory = TransformerFactory.newInstance();
		setTag(tFactory);
		Transformer transformer = tFactory.newTransformer(new StreamSource(
				fileName + ".xsl"));
		setTag(transformer);

		// Cast the Transformer object to TransformerImpl.
		if (transformer instanceof TransformerImpl) {
			TransformerImpl transformerImpl = (TransformerImpl) transformer;
			setTag(transformerImpl);
			// Register the TraceListener with a TraceManager associated
			// with the TransformerImpl.
			TraceManager trMgr = transformerImpl.getTraceManager();
			setTag(trMgr);
			trMgr.addTraceListener(ptl);

			// Perform the transformation --printing information to
			// the events log during the process.
			StreamSource streamSource = new StreamSource(fileName + ".xml");
			setTag(streamSource);

			StreamResult streamResult = new StreamResult(new java.io.FileWriter(fileName+ ".out"));
			setTag(streamResult);
			transformer.transform(streamSource,streamResult);
		}
		// Close the PrintWriter and FileWriter.
		

		
		pw.close();
		fw.close();
		

		System.out.println("**The output is in " + fileName
				+ ".out; the log is in events.log ****");
	}

	public static void main(String[] args) throws java.io.IOException,
			TransformerException, TransformerConfigurationException,
			java.util.TooManyListenersException, org.xml.sax.SAXException {

		teste();


	}
}
