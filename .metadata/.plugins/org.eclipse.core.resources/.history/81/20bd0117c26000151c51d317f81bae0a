import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import soot.G;
import soot.Local;
import soot.Timers;
import soot.Unit;
import soot.ValueBox;
import soot.jimple.internal.JAssignStmt;
import soot.jimple.internal.JIfStmt;
import soot.jimple.internal.JReturnStmt;
import soot.options.Options;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.toolkits.graph.UnitGraph;
import soot.toolkits.scalar.ArraySparseSet;
import soot.toolkits.scalar.BackwardFlowAnalysis;
import soot.toolkits.scalar.FlowSet;
import soot.toolkits.scalar.LiveLocals;

public class AecioSimpleLiveLocals implements LiveLocals {
	
	
	Map<Unit, List> unitToLocalsAfter;
	Map<Unit, List> unitToLocalsBefore;
	ArrayList<String> variables = new ArrayList<String>();
	UnitGraph graph;

	/**
	 * Computes the analysis given a UnitGraph computed from a method body. It
	 * is recommended that a ExceptionalUnitGraph (or similar) be provided for
	 * correct results in the case of exceptional control flow.
	 * 
	 * @param g
	 *            a graph on which to compute the analysis.
	 * 
	 * @see ExceptionalUnitGraph
	 */
	public AecioSimpleLiveLocals(UnitGraph graph) {
		if (Options.v().time())
			Timers.v().liveTimer.start();

		if (Options.v().verbose())
			G.v().out.println("[" + graph.getBody().getMethod().getName()
					+ "]     Constructing SimpleLiveLocals...");

		this.graph = graph;
		
		SimpleLiveLocalsAnalysis analysis = new SimpleLiveLocalsAnalysis(graph);

		if (Options.v().time())
			Timers.v().livePostTimer.start();

		// Build unitToLocals map
		{
			unitToLocalsAfter = new HashMap<Unit, List>(graph.size() * 2 + 1,
					0.7f);
			unitToLocalsBefore = new HashMap<Unit, List>(graph.size() * 2 + 1,
					0.7f);

			Iterator unitIt = graph.iterator();

			while (unitIt.hasNext()) {
				Unit s = (Unit) unitIt.next();
			
				//Get variables avaible
				Iterator variableBox = s.getUseAndDefBoxes().iterator();

				while (variableBox.hasNext()) {
					ValueBox box = (ValueBox) variableBox.next();

					if (box.getValue() instanceof Local){
						variables.add(box.getValue().toString());
					}
				}
				
				FlowSet set = (FlowSet) analysis.getFlowBefore(s);
				unitToLocalsBefore.put(s,
						Collections.unmodifiableList(set.toList()));

				set = (FlowSet) analysis.getFlowAfter(s);
				unitToLocalsAfter.put(s,
						Collections.unmodifiableList(set.toList()));
			}
		}

		if (Options.v().time())
			Timers.v().livePostTimer.end();

		if (Options.v().time())
			Timers.v().liveTimer.end();
	}
	
	//My implementation

	public List getLiveLocalsAfter(Unit s) {
		s.getTags(); //list line numbers
		return unitToLocalsAfter.get(s);
	}

	public List getLiveLocalsBefore(Unit s) {
		return unitToLocalsBefore.get(s);
	}
	
	public List getAllVariables(){
		return variables;
	}
	
	public Unit getMaxNodeUseToVariable(String variableName){
		
		Unit limitNode = null;
		ArrayList<Unit> nodesUse = new ArrayList<Unit>();
		System.out.println("Nos: =======================================================>>>>>>>>");
		for (Unit u : graph) {
			
			Unit key = u;
			System.out.println(key.toString());
			List<Local> before = getLiveLocalsBefore(u);
			List<Local> after = getLiveLocalsAfter(u);
			
			List<Local> newList = new ArrayList<>(before.size() + after.size());
		    newList.addAll(before);
		    newList.addAll(after);
		    
		    for (Local local : newList) {
				
		    	if (variableName.equals(local.getName())){
		    		nodesUse.add(key);
		    		break;
		    	}
			}
		}
		
		//Get Last Unit used
		limitNode = nodesUse.get(nodesUse.size()-1);

		if (limitNode instanceof JReturnStmt) {
			return limitNode;
		} else if (limitNode instanceof JAssignStmt) {
			// Left
			ValueBox variableUse = limitNode.getDefBoxes().get(0);

			if (!variableUse.getValue().toString().equals(variableName)) {
				return getMaxNodeUseToVariable(variableUse.getValue()
						.toString());
			} else {
				return limitNode;
			}
		
		} else if (limitNode instanceof JIfStmt){
			return limitNode;
		}else{
			return limitNode;
		}
		
		//Right
		//limitNode.getUseBoxes()	
	}
	
	
	public void free(){
		//
	}
	
	public String getVariablesGen(Unit u){
		return "";
	}
	
	public String getVariablesKill(Unit u){
		return "";
	}
	
}

class SimpleLiveLocalsAnalysis extends BackwardFlowAnalysis {
	FlowSet emptySet;
	Map<Unit, FlowSet> unitToGenerateSet;
	Map<Unit, FlowSet> unitToKillSet;
	

	SimpleLiveLocalsAnalysis(UnitGraph g) {
		super(g);

		if (Options.v().time())
			Timers.v().liveSetupTimer.start();

		emptySet = new ArraySparseSet();

		// Create kill sets.
		{
			unitToKillSet = new HashMap<Unit, FlowSet>(g.size() * 2 + 1, 0.7f);

			Iterator unitIt = g.iterator();
			while (unitIt.hasNext()) {
				Unit s = (Unit) unitIt.next();

				FlowSet killSet = emptySet.clone();

				Iterator boxIt = s.getDefBoxes().iterator();

				while (boxIt.hasNext()) {
					ValueBox box = (ValueBox) boxIt.next();

					if (box.getValue() instanceof Local)
						killSet.add(box.getValue(), killSet);
				}

				unitToKillSet.put(s, killSet);
			}
		}

		// Create generate sets
		{
			unitToGenerateSet = new HashMap<Unit, FlowSet>(g.size() * 2 + 1,
					0.7f);

			Iterator unitIt = g.iterator();

			while (unitIt.hasNext()) {
				Unit s = (Unit) unitIt.next();

				FlowSet genSet = emptySet.clone();

				Iterator boxIt = s.getUseBoxes().iterator();

				while (boxIt.hasNext()) {
					ValueBox box = (ValueBox) boxIt.next();

					if (box.getValue() instanceof Local)
						genSet.add(box.getValue(), genSet);
				}

				unitToGenerateSet.put(s, genSet);
			}
		}

		if (Options.v().time())
			Timers.v().liveSetupTimer.end();

		if (Options.v().time())
			Timers.v().liveAnalysisTimer.start();

		doAnalysis();

		if (Options.v().time())
			Timers.v().liveAnalysisTimer.end();

	}

	protected Object newInitialFlow() {
		return emptySet.clone();
	}

	protected Object entryInitialFlow() {
		return emptySet.clone();
	}

	protected void flowThrough(Object inValue, Object unit, Object outValue) {
		FlowSet in = (FlowSet) inValue, out = (FlowSet) outValue;

		// Perform kill
		in.difference(unitToKillSet.get(unit), out);

		// Perform generation
		out.union(unitToGenerateSet.get(unit), out);
	}

	protected void merge(Object in1, Object in2, Object out) {
		FlowSet inSet1 = (FlowSet) in1, inSet2 = (FlowSet) in2;

		FlowSet outSet = (FlowSet) out;

		inSet1.union(inSet2, outSet);
	}

	protected void copy(Object source, Object dest) {
		FlowSet sourceSet = (FlowSet) source, destSet = (FlowSet) dest;

		sourceSet.copy(destSet);
	}
}
