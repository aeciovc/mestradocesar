

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLFilter;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class Bench_Xalan_Trax {

	public static void teste() throws TransformerException,
			TransformerConfigurationException, IOException, SAXException,
			ParserConfigurationException, FileNotFoundException, javax.xml.parsers.ParserConfigurationException, javax.xml.parsers.FactoryConfigurationError {

		String sourceID = "xml/foo.xml";
		String xslID = "xsl/foo.xsl";

		// Example 1

		// Create a transform factory instance.
		TransformerFactory tfactory = TransformerFactory.newInstance();

		// Create a transformer for the stylesheet.
		Transformer transformer = tfactory.newTransformer(new StreamSource(
				xslID));

		// Transform the source XML to System.out.
		transformer.transform(new StreamSource(sourceID), new StreamResult(
				new OutputStreamWriter(System.out)));

		// Example 2

		// Create a transform factory instance.
		TransformerFactory tfactory2 = TransformerFactory.newInstance();

		// Create a transformer for the stylesheet.
		Transformer transformer222 = tfactory2.newTransformer(new StreamSource(
				xslID));

		// Transform the source XML to foo.out.
		transformer.transform(new StreamSource(new File(sourceID)),
				new StreamResult(new File("foo.out")));

		// Example 3
		// Create a transform factory instance.
		TransformerFactory tfactory3 = TransformerFactory.newInstance();

		InputStream xslIS = new BufferedInputStream(new FileInputStream(xslID));
		StreamSource xslSource = new StreamSource(xslIS);
		// Note that if we don't do this, relative URLs can not be resolved
		// correctly!
		xslSource.setSystemId(xslID);

		// Create a transformer for the stylesheet.
		Transformer transformer1 = tfactory3.newTransformer(xslSource);

		InputStream xmlIS = new BufferedInputStream(new FileInputStream(
				sourceID));
		StreamSource xmlSource = new StreamSource(xmlIS);
		// Note that if we don't do this, relative URLs can not be resolved
		// correctly!
		xmlSource.setSystemId(sourceID);

		// Transform the source XML to System.out.
		transformer1.transform(xmlSource, new StreamResult(
				new OutputStreamWriter(System.out)));

		// Example 4
		// Create a transform factory instance.
		TransformerFactory tfactory4 = TransformerFactory.newInstance();

		// Note that in this case the XML encoding can not be processed!
		Reader xslReader = new BufferedReader(new InputStreamReader(
				new FileInputStream(xslID), "UTF-8"));
		StreamSource xslSource3 = new StreamSource(xslReader);
		// Note that if we don't do this, relative URLs can not be resolved
		// correctly!
		xslSource3.setSystemId(xslID);

		// Create a transformer for the stylesheet.
		Transformer transformer3 = tfactory4.newTransformer(xslSource3);

		// Note that in this case the XML encoding can not be processed!
		Reader xmlReader = new BufferedReader(new InputStreamReader(
				new FileInputStream(sourceID), "UTF-8"));
		StreamSource xmlSource3 = new StreamSource(xmlReader);
		// Note that if we don't do this, relative URLs can not be resolved
		// correctly!
		xmlSource3.setSystemId(sourceID);

		// Transform the source XML to System.out.
		transformer3.transform(xmlSource3, new StreamResult(
				new OutputStreamWriter(System.out)));

		// Example 4 exampleUseTemplatesObj

		TransformerFactory tfactory44 = TransformerFactory.newInstance();

		// Create a templates object, which is the processed,
		// thread-safe representation of the stylesheet.
		Templates templates = tfactory44.newTemplates(new StreamSource(xslID));

		// Illustrate the fact that you can make multiple transformers
		// from the same template.
		Transformer transformer14 = templates.newTransformer();
		Transformer transformer2 = templates.newTransformer();

		System.out.println("\n\n----- transform of " + sourceID + " -----");

		transformer14.transform(new StreamSource(sourceID), new StreamResult(
				new OutputStreamWriter(System.out)));

		System.out.println("\n\n----- transform of " + sourceID + " -----");

		transformer2.transform(new StreamSource(sourceID), new StreamResult(
				new OutputStreamWriter(System.out)));

		// Example 5 - exampleContentHandlerToContentHandler

		TransformerFactory tfactory5 = TransformerFactory.newInstance();

		// Does this factory support SAX features?
		if (tfactory5.getFeature(SAXSource.FEATURE)) {
			// If so, we can safely cast.
			SAXTransformerFactory stfactory = ((SAXTransformerFactory) tfactory5);

			// A TransformerHandler is a ContentHandler that will listen for
			// SAX events, and transform them to the result.
			TransformerHandler handler = stfactory
					.newTransformerHandler(new StreamSource(xslID));

			Result result = new SAXResult(new ExampleContentHandler());
			handler.setResult(result);

			// Create a reader, and set it's content handler to be the
			// TransformerHandler.
			XMLReader reader = null;

			// Use JAXP1.1 ( if possible )
			
				javax.xml.parsers.SAXParserFactory factory = javax.xml.parsers.SAXParserFactory
						.newInstance();
				factory.setNamespaceAware(true);
				javax.xml.parsers.SAXParser jaxpParser = factory.newSAXParser();
				reader = jaxpParser.getXMLReader();

			
			if (reader == null)
				reader = XMLReaderFactory.createXMLReader();
			reader.setContentHandler(handler);

			// It's a good idea for the parser to send lexical events.
			// The TransformerHandler is also a LexicalHandler.
			reader.setProperty("http://xml.org/sax/properties/lexical-handler",
					handler);

			// Parse the source XML, and send the parse events to the
			// TransformerHandler.
			reader.parse(sourceID);
		} else {
			System.out
					.println("Can't do exampleContentHandlerToContentHandler because tfactory is not a SAXTransformerFactory");
		}

		// Example 6 - exampleXMLReader

		TransformerFactory tfactory6 = TransformerFactory.newInstance();
		if (tfactory6.getFeature(SAXSource.FEATURE)) {
			XMLReader reader = ((SAXTransformerFactory) tfactory6)
					.newXMLFilter(new StreamSource(xslID));

			ExampleContentHandler a = new ExampleContentHandler();
			reader.setContentHandler(new ExampleContentHandler());

			reader.parse(new InputSource(sourceID));
		} else
			System.out.println("tfactory does not support SAX features!");

		// Example 7 - exampleXMLFilter

		TransformerFactory tfactory7 = TransformerFactory.newInstance();

		XMLReader reader = null;

		// Use JAXP1.1 ( if possible )

			javax.xml.parsers.SAXParserFactory factory = javax.xml.parsers.SAXParserFactory
					.newInstance();
			factory.setNamespaceAware(true);
			javax.xml.parsers.SAXParser jaxpParser = factory.newSAXParser();
			reader = jaxpParser.getXMLReader();

		if (reader == null)
			reader = XMLReaderFactory.createXMLReader();
		// The transformer will use a SAX parser as it's reader.
		reader.setContentHandler(new ExampleContentHandler());
		
	
			reader.setFeature("http://xml.org/sax/features/namespace-prefixes",
					true);
			reader.setFeature(
					"http://apache.org/xml/features/validation/dynamic", true);


		XMLFilter filter = ((SAXTransformerFactory) tfactory7)
				.newXMLFilter(new StreamSource(xslID));

		filter.setParent(reader);

		// Now, when you call transformer.parse, it will set itself as
		// the content handler for the parser object (it's "parent"), and
		// will then call the parse method on the parser.
		filter.parse(new InputSource(sourceID));

		// Example 8 - exampleXMLFilterChain

		String xsl1 = "xsl/foo.xsl";
		String xsl2 = "xsl/foo2.xsl";
		String xsl3 = "xsl/foo3.xsl";

		TransformerFactory tfactory8 = TransformerFactory.newInstance();

		Templates stylesheet1 = tfactory.newTemplates(new StreamSource(xsl1));
		Transformer transformer8 = stylesheet1.newTransformer();

		// If one success, assume all will succeed.
		if (tfactory.getFeature(SAXSource.FEATURE)) {
			SAXTransformerFactory stf = (SAXTransformerFactory) tfactory;
			XMLReader reader1 = null;

			// Use JAXP1.1 ( if possible )

				javax.xml.parsers.SAXParserFactory factory8 = javax.xml.parsers.SAXParserFactory
						.newInstance();
				factory8.setNamespaceAware(true);
				javax.xml.parsers.SAXParser jaxpParser8 = factory8.newSAXParser();
				reader1 = jaxpParser8.getXMLReader();


			if (reader == null)
				reader = XMLReaderFactory.createXMLReader();

			XMLFilter filter1 = stf.newXMLFilter(new StreamSource(xsl1));
			XMLFilter filter2 = stf.newXMLFilter(new StreamSource(xsl2));
			XMLFilter filter3 = stf.newXMLFilter(new StreamSource(xsl3));

			if (null != filter1) // If one success, assume all were success.
			{
				// transformer1 will use a SAX parser as it's reader.
				filter1.setParent(reader);

				// transformer2 will use transformer1 as it's reader.
				filter2.setParent(filter1);

				// transform3 will use transform2 as it's reader.
				filter3.setParent(filter2);

				filter3.setContentHandler(new ExampleContentHandler());
				// filter3.setContentHandler(new
				// org.xml.sax.helpers.DefaultHandler());

				// Now, when you call transformer3 to parse, it will set
				// itself as the ContentHandler for transform2, and
				// call transform2.parse, which will set itself as the
				// content handler for transform1, and call transform1.parse,
				// which will set itself as the content listener for the
				// SAX parser, and call parser.parse(new
				// InputSource("xml/foo.xml")).
				filter3.parse(new InputSource(sourceID));
			} else {
				System.out.println("Can't do exampleXMLFilter because "
						+ "tfactory doesn't support asXMLFilter()");
			}
		} else {
			System.out.println("Can't do exampleXMLFilter because "
					+ "tfactory is not a SAXTransformerFactory");
		}

		// Exmple 9 - exampleDOM2DOM

		TransformerFactory tfactory9 = TransformerFactory.newInstance();

		if (tfactory.getFeature(DOMSource.FEATURE)) {
			Templates templates1;

			{
				DocumentBuilderFactory dfactory = DocumentBuilderFactory
						.newInstance();
				dfactory.setNamespaceAware(true);
				DocumentBuilder docBuilder = dfactory.newDocumentBuilder();
				org.w3c.dom.Document outNode9 = docBuilder.newDocument();
				Node doc = docBuilder.parse(new InputSource(xslID));

				DOMSource dsource = new DOMSource(doc);
				// If we don't do this, the transformer won't know how to
				// resolve relative URLs in the stylesheet.
				dsource.setSystemId(xslID);

				templates1 = tfactory.newTemplates(dsource);
			}

			Transformer transformer9 = templates1.newTransformer();
			DocumentBuilderFactory dfactory = DocumentBuilderFactory
					.newInstance();
			// Note you must always setNamespaceAware when building .xsl
			// stylesheets
			dfactory.setNamespaceAware(true);
			DocumentBuilder docBuilder = dfactory.newDocumentBuilder();
			org.w3c.dom.Document outNode = docBuilder.newDocument();
			Node doc = docBuilder.parse(new InputSource(sourceID));

			transformer9.transform(new DOMSource(doc), new DOMResult(outNode));

			Transformer serializer = tfactory.newTransformer();
			serializer.transform(new DOMSource(outNode), new StreamResult(
					new OutputStreamWriter(System.out)));

			System.out.println(outNode);
		} else {
			throw new org.xml.sax.SAXNotSupportedException(
					"DOM node processing not supported!");
		}
		
		//Example 10 - exampleParam
			
			TransformerFactory tfactory100 = TransformerFactory.newInstance();
			Templates templates100a = tfactory100.newTemplates(new StreamSource(xslID));
			Transformer transformer1abc = templates100a.newTransformer();
			Transformer transformer2def = templates100a.newTransformer();

			transformer1abc.setParameter("a-param", "hello to you!");
			transformer1abc.transform(new StreamSource(sourceID), new StreamResult(
					new OutputStreamWriter(System.out)));

			System.out.println("\n=========");

			transformer2def.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer2def.transform(new StreamSource(sourceID), new StreamResult(
					new OutputStreamWriter(System.out)));

		
		//Example 11 - exampleTransformerReuse
		TransformerFactory tfactory11 = TransformerFactory.newInstance();

		// Create a transformer for the stylesheet.
		Transformer transformer11 = tfactory11.newTransformer(new StreamSource(
				xslID));

		transformer11.setParameter("a-param", "hello to you!");

		// Transform the source XML to System.out.
		transformer11.transform(new StreamSource(sourceID), new StreamResult(
				new OutputStreamWriter(System.out)));

		System.out.println("\n=========\n");

		transformer11.setParameter("a-param", "hello to me!");
		transformer11.setOutputProperty(OutputKeys.INDENT, "yes");

		// Transform the source XML to System.out.
		transformer11.transform(new StreamSource(sourceID), new StreamResult(
				new OutputStreamWriter(System.out)));
		
		//Example 12 - exampleOutputProperties
		
		TransformerFactory tfactory12 = TransformerFactory.newInstance();
		Templates templates12 = tfactory12.newTemplates(new StreamSource(xslID));
		Properties oprops = templates12.getOutputProperties();

		oprops.put(OutputKeys.INDENT, "yes");

		Transformer transformer12 = templates12.newTransformer();

		transformer12.setOutputProperties(oprops);
		transformer12.transform(new StreamSource(sourceID), new StreamResult(
				new OutputStreamWriter(System.out)));

		
		//Example 13 - exampleUseAssociated
		
			TransformerFactory tfactory13 = TransformerFactory.newInstance();
			// The DOM tfactory will have it's own way, based on DOM2,
			// of getting associated stylesheets.
			if (tfactory13 instanceof SAXTransformerFactory) {
				SAXTransformerFactory stf = ((SAXTransformerFactory) tfactory13);
				Source sources = stf.getAssociatedStylesheet(new StreamSource(
						sourceID), null, null, null);

				if (null != sources) {
					Transformer transformer13 = tfactory.newTransformer(sources);

					transformer13.transform(new StreamSource(sourceID),
							new StreamResult(new OutputStreamWriter(System.out)));
				} else {
					System.out.println("Can't find the associated stylesheet!");
				}
			}
		
		//Example 14 - exampleContentHandler2DOM
		
		TransformerFactory tfactory14 = TransformerFactory.newInstance();
		org.w3c.dom.Document outNode = null;

		// Make sure the transformer factory we obtained supports both
		// DOM and SAX.
		if (tfactory14.getFeature(SAXSource.FEATURE)
				&& tfactory14.getFeature(DOMSource.FEATURE)) {
			// We can now safely cast to a SAXTransformerFactory.
			SAXTransformerFactory sfactory = (SAXTransformerFactory) tfactory14;

			// Create an Document node as the root for the output.
			DocumentBuilderFactory dfactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = dfactory.newDocumentBuilder();
			outNode = docBuilder.newDocument();

			// Create a ContentHandler that can liston to SAX events
			// and transform the output to DOM nodes.
			TransformerHandler handler = sfactory
					.newTransformerHandler(new StreamSource(xslID));
			handler.setResult(new DOMResult(outNode));

			// Create a reader and set it's ContentHandler to be the
			// transformer.
			XMLReader reader14 = null;

			// Use JAXP1.1 ( if possible )
				javax.xml.parsers.SAXParserFactory factory14 = javax.xml.parsers.SAXParserFactory
						.newInstance();
				factory14.setNamespaceAware(true);
				javax.xml.parsers.SAXParser jaxpParser14 = factory14.newSAXParser();
				reader14 = jaxpParser14.getXMLReader();

			if (reader14 == null)
				reader14 = XMLReaderFactory.createXMLReader();
			reader14.setContentHandler(handler);
			reader14.setProperty("http://xml.org/sax/properties/lexical-handler",
					handler);

			// Send the SAX events from the parser to the transformer,
			// and thus to the DOM tree.
			reader14.parse(sourceID);

			// Serialize the node for diagnosis.
			System.out.println(outNode);
		} else {
			System.out
					.println("Can't do exampleContentHandlerToContentHandler because tfactory is not a SAXTransformerFactory");
		}
	
		//Example 15 - exampleSerializeNode
		
		// This creates a transformer that does a simple identity transform,
		// and thus can be used for all intents and purposes as a serializer.
		Transformer serializer15 = tfactory.newTransformer();

		serializer15.setOutputProperty(OutputKeys.INDENT, "yes");
		serializer15.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		serializer15.transform(new DOMSource(outNode), new StreamResult(
				new OutputStreamWriter(System.out)));
	
		
		//Example 16 - exampleAsSerializer
		
		DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dfactory.newDocumentBuilder();
		org.w3c.dom.Document outNode16 = docBuilder.newDocument();
		Node doc = docBuilder.parse(new InputSource(sourceID));

		TransformerFactory tfactory16 = TransformerFactory.newInstance();

		// This creates a transformer that does a simple identity transform,
		// and thus can be used for all intents and purposes as a serializer.
		Transformer serializer = tfactory16.newTransformer();

		Properties oprops16 = new Properties();
		oprops16.put("method", "html");
		serializer.setOutputProperties(oprops16);
		serializer.transform(new DOMSource(doc), new StreamResult(
				new OutputStreamWriter(System.out)));
		
		
		//Example 17 - 

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws TransformerException,
			TransformerConfigurationException, IOException, SAXException,
			ParserConfigurationException, FileNotFoundException {
		// TODO Auto-generated method stub
		teste();
	}
	


}
