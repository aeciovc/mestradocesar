
public class Long2 {

	 static int countInstances = 0;
	 
	 public Long value;
	 
	 public Long2(Long value){
		 this.value = value;
		 countInstances = countInstances + 1;
	 }
	 public Long2(){
		 countInstances = countInstances + 1;
	 }
	 
	 public static int getAllocated(){
		 return countInstances;
	 }
 
}
