package dk.brics.soot.analyses;

import java.util.HashMap;
import java.util.List;

import soot.Body;
import soot.Local;
import soot.RefLikeType;
import soot.Unit;
import soot.toolkits.graph.UnitGraph;
import soot.toolkits.scalar.ArraySparseSet;
import soot.toolkits.scalar.FlowSet;
import soot.toolkits.scalar.ForwardBranchedFlowAnalysis;

public class NullnessAnalysis extends ForwardBranchedFlowAnalysis<FlowSet> {

	HashMap unitToGenerateSet;
	ArraySparseSet emptySet;
	ArraySparseSet fullSet;

	public NullnessAnalysis(UnitGraph graph) {
		super(graph);

		unitToGenerateSet = new HashMap();
		Body b = graph.getBody();

		emptySet = new ArraySparseSet();
		fullSet = new ArraySparseSet();
		for (Local l : b.getLocals()) {
			if (l.getType() instanceof RefLikeType)
				fullSet.add(l);
		}
	}

	@Override
	protected void flowThrough(FlowSet in, Unit s, List<FlowSet> fallOut,
			List<FlowSet> branchOuts) {
		FlowSet copys;
		in.copy(copys);

	}

	@Override
	protected FlowSet newInitialFlow() {
		return fullSet.clone();
	}

	@Override
	protected FlowSet entryInitialFlow() {
		return emptySet.clone();
	}

	@Override
	protected void merge(FlowSet srcSet1, FlowSet srcSet2, FlowSet destSet) {
		srcSet1.intersection(srcSet2, destSet);

	}

	@Override
	protected void copy(FlowSet source, FlowSet dest) {
		source.copy(dest);

	}

}
