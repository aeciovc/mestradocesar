
/**
 * Sample for demonstrating Xalan "trace" interface. Usage: run in Trace
 * directory: java Trace For an extensions trace sample, run in extensions
 * directory: java Trace 3-java-namespace
 */
public class SieveJNI {

	static int countDealocationsLOOF = 0;

 	static {
      System.loadLibrary("sieveJNI"); // Load native library at runtime
                                   // hello.dll (Windows) or libhello.so (Unixes)
   	}

   	  // Declare a native method sayHello() that receives nothing and returns void
   	private static native void sayHello();
   	private static native void setTag(Object o);


	public static void teste() {
		Integer N = Integer.parseInt("100000");
		setTag(N);

		Boolean[] isPrime = new Boolean[N + 1];
		for (Integer i = 2; i <= N; i++) {
			setTag(i);
			isPrime[i] = true;
			countLOOFDealocations("[LOOF] i-Integer"); 
		}

		for (Integer b = 2; b * b <= N; b++) {
			setTag(b);
			if (isPrime[b]) {
				for (Integer j = b; b * j <= N; j++) {
					setTag(j);
					isPrime[b * j] = false;
					countLOOFDealocations("[LOOF] j-Integer"); 
				}
			
			}
			countLOOFDealocations("[LOOF] b-Integer"); 
			
		}

		Integer primes = 0;
		for (Integer a = 2; a <= N; a++) {
			setTag(a);
			if (isPrime[a]){
				primes++;
				setTag(primes);
				countLOOFDealocations("[LOOF] a-Integer"); //LOOF
			}
		}
		
		
		System.out.println("The number of primes <= " + N + " is " + primes);
		countLOOFDealocations("[LOOF] N-Integer"); //LOOF
		countLOOFDealocations("[LOOF] primes-Integer"); //LOOF

	}

	public static void main(String[] args){

		teste();
		System.out.println("Quantidade de desalocações LOOF:"+ countDealocationsLOOF);


	}


	public static void countLOOFDealocations(String text){    	
    	countDealocationsLOOF = countDealocationsLOOF + 1;
    }
}
