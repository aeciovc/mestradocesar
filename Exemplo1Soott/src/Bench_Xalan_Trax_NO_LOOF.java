

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.XMLFilter;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class Bench_Xalan_Trax_NO_LOOF {

	 

	   public static void teste() throws javax.xml.transform.TransformerException, javax.xml.transform.TransformerConfigurationException, java.io.IOException, org.xml.sax.SAXException, javax.xml.parsers.ParserConfigurationException, java.io.FileNotFoundException, javax.xml.parsers.ParserConfigurationException, javax.xml.parsers.FactoryConfigurationError
	    {

	        String r0, r1, r2, r3, r4, r5, r6;
	        TransformerFactory r8, r10, r12, r14, r16, r18, r20, r22, r24, r26, r28, r30, r32, r34, r36, r38;
	        Transformer r40, r42, r44, r46, r48, r50, r52, r54, r55, r57, r59, r61, r63, r65, r67, r69;
	        StreamSource r70, r71, r72, r73, r75, r77, r79, r81, r82, r83, r84, r85, r86, r87, r88, r89, r90, r91, r92, r93, r94, r95, r96, r97, r98, r99, r100, r101, r102;
	        StreamResult r103, r104, r105, r106, r107, r108, r109, r110, r111, r112, r113, r114, r115, r116, r117;
	        OutputStreamWriter r118, r119, r120, r121, r122, r123, r124, r125, r126, r127, r128, r129, r130, r131;
	        PrintStream r132, r133, r134, r135, r136, r137, r138, r139, r140, r141, r142, r143, r144, r145, r146, r147, r148, r149, r150, r151, r152, r153, r154, r155, r156, r157;
	        File r158, r159;
	        BufferedInputStream r162, r163;
	        FileInputStream r164, r165, r166, r167;
	        BufferedReader r170, r171;
	        InputStreamReader r172, r173;
	        Templates r175, r177, r179, r181, r183;
	        StringBuffer r184, r185;
	        boolean z0, z1, z2, z3, z4, z5, z6;
	        SAXTransformerFactory r186, r187, r188, r189, r190, r286;
	        TransformerHandler r191, r193;
	        SAXResult r195;
	        ExampleContentHandler r196, r198, r199, r200, r201;
	        XMLReader r202, r203, r204, r205, r206, r207, r208, r209, r277, r281, r291;
	        SAXParserFactory r210, r212, r214, r216;
	        SAXParser r217, r219, r221, r223;
	        XMLFilter r224, r226, r228, r230, r232;
	        InputSource r233, r234, r235, r236, r237, r238;
	        DocumentBuilderFactory r239, r240, r241, r243;
	        DocumentBuilder r244, r245, r246, r248;
	        Document r250, r251, r252, r253, r255, r257, r258, r287;
	        DOMSource r261, r262, r263, r264, r265;
	        DOMResult r266, r267;
	        SAXNotSupportedException r268;
	        Properties r270, r272;
	        Source r276;
	        r0 = "xml/foo.xml";
	        r1 = "xsl/foo.xsl";
	        r8 = TransformerFactory.newInstance();
	        r70 = new StreamSource(r1);
	        r40 = r8.newTransformer(r70);
	        r71 = new StreamSource(r0);
	        r132 = System.out;
	        r118 = new OutputStreamWriter(r132);
	        r103 = new StreamResult(r118);
	        r40.transform(r71, r103);
	        r10 = TransformerFactory.newInstance();
	        r72 = new StreamSource(r1);
	        r42 = r10.newTransformer(r72);
	        r158 = new File(r0);
	        r73 = new StreamSource(r158);
	        r159 = new File("foo.out");
	        r104 = new StreamResult(r159);
	        r40.transform(r73, r104);
	        r12 = TransformerFactory.newInstance();
	        r164 = new FileInputStream(r1);
	        r162 = new BufferedInputStream(r164);
	        r75 = new StreamSource(r162);
	        r75.setSystemId(r1);
	        r44 = r12.newTransformer(r75);
	        r165 = new FileInputStream(r0);
	        r163 = new BufferedInputStream(r165);
	        r77 = new StreamSource(r163);
	        r77.setSystemId(r0);
	        r133 = System.out;
	        r119 = new OutputStreamWriter(r133);
	        r105 = new StreamResult(r119);
	        r44.transform(r77, r105);
	        r14 = TransformerFactory.newInstance();
	        r166 = new FileInputStream(r1);
	        r172 = new InputStreamReader(r166, "UTF-8");
	        r170 = new BufferedReader(r172);
	        r79 = new StreamSource(r170);
	        r79.setSystemId(r1);
	        r46 = r14.newTransformer(r79);
	        r167 = new FileInputStream(r0);
	        r173 = new InputStreamReader(r167, "UTF-8");
	        r171 = new BufferedReader(r173);
	        r81 = new StreamSource(r171);
	        r81.setSystemId(r0);
	        r134 = System.out;
	        r120 = new OutputStreamWriter(r134);
	        r106 = new StreamResult(r120);
	        r46.transform(r81, r106);
	        r16 = TransformerFactory.newInstance();
	        r82 = new StreamSource(r1);
	        r175 = r16.newTemplates(r82);
	        r48 = r175.newTransformer();
	        r50 = r175.newTransformer();
	        r135 = System.out;
	        r184 = new StringBuffer();
	        r184.append("\n\n----- transform of ");
	        r184.append(r0);
	        r184.append(" -----");
	        r2 = r184.toString();
	        r135.println(r2);
	        r83 = new StreamSource(r0);
	        r136 = System.out;
	        r121 = new OutputStreamWriter(r136);
	        r107 = new StreamResult(r121);
	        r48.transform(r83, r107);
	        r137 = System.out;
	        r185 = new StringBuffer();
	        r185.append("\n\n----- transform of ");
	        r185.append(r0);
	        r185.append(" -----");
	        r3 = r185.toString();
	        r137.println(r3);
	        r84 = new StreamSource(r0);
	        r138 = System.out;
	        r122 = new OutputStreamWriter(r138);
	        r108 = new StreamResult(r122);
	        r50.transform(r84, r108);
	        r18 = TransformerFactory.newInstance();
	        z0 = r18.getFeature("http://javax.xml.transform.sax.SAXSource/feature");

	        if ( ! (z0))
	        {
	            r139 = System.out;
	            r139.println("Can\'t do exampleContentHandlerToContentHandler because tfactory is not a SAXTransformerFactory");
	        }
	        else
	        {
	            r186 = (SAXTransformerFactory) r18;
	            r85 = new StreamSource(r1);
	            r191 = r186.newTransformerHandler(r85);
	            r196 = new ExampleContentHandler();
	            r195 = new SAXResult(r196);
	            r191.setResult(r195);
	            r210 = SAXParserFactory.newInstance();
	            r210.setNamespaceAware(true);
	            r217 = r210.newSAXParser();
	            r202 = r217.getXMLReader();
	            r277 = r202;

	            if (r277 == null)
	            {
	                r203 = XMLReaderFactory.createXMLReader();
	                r277 = r203;
	            }

	            r277.setContentHandler(r191);
	            r277.setProperty("http://xml.org/sax/properties/lexical-handler", r191);
	            r277.parse(r0);
	        }

	        r20 = TransformerFactory.newInstance();
	        z1 = r20.getFeature("http://javax.xml.transform.sax.SAXSource/feature");

	        if ( ! (z1))
	        {
	            r140 = System.out;
	            r140.println("tfactory does not support SAX features!");
	        }
	        else
	        {
	            r187 = (SAXTransformerFactory) r20;
	            r86 = new StreamSource(r1);
	            r224 = r187.newXMLFilter(r86);
	            r198 = new ExampleContentHandler();
	            r199 = new ExampleContentHandler();
	            r224.setContentHandler(r199);
	            r233 = new InputSource(r0);
	            r224.parse(r233);
	        }

	        r22 = TransformerFactory.newInstance();
	        r212 = SAXParserFactory.newInstance();
	        r212.setNamespaceAware(true);
	        r219 = r212.newSAXParser();
	        r204 = r219.getXMLReader();
	        r281 = r204;

	        if (r281 == null)
	        {
	            r205 = XMLReaderFactory.createXMLReader();
	            r281 = r205;
	        }

	        r200 = new ExampleContentHandler();
	        r281.setContentHandler(r200);
	        r281.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
	        r281.setFeature("http://apache.org/xml/features/validation/dynamic", true);
	        r188 = (SAXTransformerFactory) r22;
	        r87 = new StreamSource(r1);
	        r226 = r188.newXMLFilter(r87);
	        r226.setParent(r281);
	        r234 = new InputSource(r0);
	        r226.parse(r234);
	        r4 = "xsl/foo.xsl";
	        r5 = "xsl/foo2.xsl";
	        r6 = "xsl/foo3.xsl";
	        r24 = TransformerFactory.newInstance();
	        r88 = new StreamSource(r4);
	        r177 = r8.newTemplates(r88);
	        r52 = r177.newTransformer();
	        z2 = r8.getFeature("http://javax.xml.transform.sax.SAXSource/feature");

	        if ( ! (z2))
	        {
	            r142 = System.out;
	            r142.println("Can\'t do exampleXMLFilter because tfactory is not a SAXTransformerFactory");
	        }
	        else
	        {
	            r189 = (SAXTransformerFactory) r8;
	            r214 = SAXParserFactory.newInstance();
	            r214.setNamespaceAware(true);
	            r221 = r214.newSAXParser();
	            r206 = r221.getXMLReader();

	            if (r281 == null)
	            {
	                r207 = XMLReaderFactory.createXMLReader();
	                r281 = r207;
	            }

	            r89 = new StreamSource(r4);
	            r228 = r189.newXMLFilter(r89);
	            r90 = new StreamSource(r5);
	            r230 = r189.newXMLFilter(r90);
	            r91 = new StreamSource(r6);
	            r232 = r189.newXMLFilter(r91);

	            if (null != r228)
	            {
	                r228.setParent(r281);
	                r230.setParent(r228);
	                r232.setParent(r230);
	                r201 = new ExampleContentHandler();
	                r232.setContentHandler(r201);
	                r235 = new InputSource(r0);
	                r232.parse(r235);
	            }
	            else
	            {
	                r141 = System.out;
	                r141.println("Can\'t do exampleXMLFilter because tfactory doesn\'t support asXMLFilter()");
	            }
	        }

	        r26 = TransformerFactory.newInstance();
	        z3 = r8.getFeature("http://javax.xml.transform.dom.DOMSource/feature");

	        if (z3)
	        {
	            r239 = DocumentBuilderFactory.newInstance();
	            r239.setNamespaceAware(true);
	            r244 = r239.newDocumentBuilder();
	            r250 = r244.newDocument();
	            r236 = new InputSource(r1);
	            r251 = r244.parse(r236);
	            r261 = new DOMSource(r251);
	            r261.setSystemId(r1);
	            r179 = r8.newTemplates(r261);
	            r54 = r179.newTransformer();
	            r240 = DocumentBuilderFactory.newInstance();
	            r240.setNamespaceAware(true);
	            r245 = r240.newDocumentBuilder();
	            r252 = r245.newDocument();
	            r237 = new InputSource(r0);
	            r253 = r245.parse(r237);
	            r262 = new DOMSource(r253);
	            r266 = new DOMResult(r252);
	            r54.transform(r262, r266);
	            r55 = r8.newTransformer();
	            r263 = new DOMSource(r252);
	            r143 = System.out;
	            r123 = new OutputStreamWriter(r143);
	            r109 = new StreamResult(r123);
	            r55.transform(r263, r109);
	            r144 = System.out;
	            r144.println(r252);
	            r28 = TransformerFactory.newInstance();
	            r92 = new StreamSource(r1);
	            r181 = r28.newTemplates(r92);
	            r57 = r181.newTransformer();
	            r59 = r181.newTransformer();
	            r57.setParameter("a-param", "hello to you!");
	            r93 = new StreamSource(r0);
	            r145 = System.out;
	            r124 = new OutputStreamWriter(r145);
	            r110 = new StreamResult(r124);
	            r57.transform(r93, r110);
	            r146 = System.out;
	            r146.println("\n=========");
	            r59.setOutputProperty("indent", "yes");
	            r94 = new StreamSource(r0);
	            r147 = System.out;
	            r125 = new OutputStreamWriter(r147);
	            r111 = new StreamResult(r125);
	            r59.transform(r94, r111);
	            r30 = TransformerFactory.newInstance();
	            r95 = new StreamSource(r1);
	            r61 = r30.newTransformer(r95);
	            r61.setParameter("a-param", "hello to you!");
	            r96 = new StreamSource(r0);
	            r148 = System.out;
	            r126 = new OutputStreamWriter(r148);
	            r112 = new StreamResult(r126);
	            r61.transform(r96, r112);
	            r149 = System.out;
	            r149.println("\n=========\n");
	            r61.setParameter("a-param", "hello to me!");
	            r61.setOutputProperty("indent", "yes");
	            r97 = new StreamSource(r0);
	            r150 = System.out;
	            r127 = new OutputStreamWriter(r150);
	            r113 = new StreamResult(r127);
	            r61.transform(r97, r113);
	            r32 = TransformerFactory.newInstance();
	            r98 = new StreamSource(r1);
	            r183 = r32.newTemplates(r98);
	            r270 = r183.getOutputProperties();
	            r270.put("indent", "yes");
	            r63 = r183.newTransformer();
	            r63.setOutputProperties(r270);
	            r99 = new StreamSource(r0);
	            r151 = System.out;
	            r128 = new OutputStreamWriter(r151);
	            r114 = new StreamResult(r128);
	            r63.transform(r99, r114);
	            r34 = TransformerFactory.newInstance();
	            z4 = r34 instanceof SAXTransformerFactory;

	            if (z4)
	            {
	                r286 = (SAXTransformerFactory) r34;
	                r100 = new StreamSource(r0);
	                r276 = r286.getAssociatedStylesheet(r100, null, null, null);

	                if (null != r276)
	                {
	                    r65 = r8.newTransformer(r276);
	                    r101 = new StreamSource(r0);
	                    r152 = System.out;
	                    r129 = new OutputStreamWriter(r152);
	                    r115 = new StreamResult(r129);
	                    r65.transform(r101, r115);
	                }
	                else
	                {
	                    r153 = System.out;
	                    r153.println("Can\'t find the associated stylesheet!");
	                }
	            }

	            r36 = TransformerFactory.newInstance();
	            r287 = null;
	            z5 = r36.getFeature("http://javax.xml.transform.sax.SAXSource/feature");

	            label_0:
	            {
	                if (z5)
	                {
	                    z6 = r36.getFeature("http://javax.xml.transform.dom.DOMSource/feature");

	                    if (z6)
	                    {
	                        r190 = (SAXTransformerFactory) r36;
	                        r241 = DocumentBuilderFactory.newInstance();
	                        r246 = r241.newDocumentBuilder();
	                        r255 = r246.newDocument();
	                        r287 = r255;
	                        r102 = new StreamSource(r1);
	                        r193 = r190.newTransformerHandler(r102);
	                        r267 = new DOMResult(r287);
	                        r193.setResult(r267);
	                        r216 = SAXParserFactory.newInstance();
	                        r216.setNamespaceAware(true);
	                        r223 = r216.newSAXParser();
	                        r208 = r223.getXMLReader();
	                        r291 = r208;

	                        if (r291 == null)
	                        {
	                            r209 = XMLReaderFactory.createXMLReader();
	                            r291 = r209;
	                        }

	                        r291.setContentHandler(r193);
	                        r291.setProperty("http://xml.org/sax/properties/lexical-handler", r193);
	                        r291.parse(r0);
	                        r154 = System.out;
	                        r154.println(r287);
	                        break label_0;
	                    }
	                }

	                r155 = System.out;
	                r155.println("Can\'t do exampleContentHandlerToContentHandler because tfactory is not a SAXTransformerFactory");
	            } //end label_0:


	            r67 = r8.newTransformer();
	            r67.setOutputProperty("indent", "yes");
	            r67.setOutputProperty("omit-xml-declaration", "yes");
	            r264 = new DOMSource(r287);
	            r156 = System.out;
	            r130 = new OutputStreamWriter(r156);
	            r116 = new StreamResult(r130);
	            r67.transform(r264, r116);
	            r243 = DocumentBuilderFactory.newInstance();
	            r248 = r243.newDocumentBuilder();
	            r257 = r248.newDocument();
	            r238 = new InputSource(r0);
	            r258 = r248.parse(r238);
	            r38 = TransformerFactory.newInstance();
	            r69 = r38.newTransformer();
	            r272 = new Properties();
	            r272.put("method", "html");
	            r69.setOutputProperties(r272);
	            r265 = new DOMSource(r258);
	            r157 = System.out;
	            r131 = new OutputStreamWriter(r157);
	            r117 = new StreamResult(r131);
	            r69.transform(r265, r117);
	            return;
	        }

	        r268 = new SAXNotSupportedException("DOM node processing not supported!");
	        throw r268;
	    }

	/**
	 * @param args
	 */
	public static void main(String[] args) throws TransformerException,
			TransformerConfigurationException, IOException, SAXException,
			ParserConfigurationException, FileNotFoundException {
		// TODO Auto-generated method stub
		teste();
	}

}
