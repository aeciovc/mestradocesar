
/**
 * Sample for demonstrating Xalan "trace" interface. Usage: run in Trace
 * directory: java Trace For an extensions trace sample, run in extensions
 * directory: java Trace 3-java-namespace
 */
public class SieveJNI {

	static boolean result1Deallocated = false;
	static int countDealocationsLOOF = 0;

 	static {
      System.loadLibrary("sieveJNI"); // Load native library at runtime
                                   // hello.dll (Windows) or libhello.so (Unixes)
   	}

   	  // Declare a native method sayHello() that receives nothing and returns void
   	private static native void sayHello();
   	private static native void setTag(Object o);


	public static void teste() {

		String results1, results2;
		results1 = "Running Sieve benchmark.";
		results2 = "This will take about 10 seconds.";
		setTag(results1);
		setTag(results2);

		Integer SIZE = 8190;
		setTag(SIZE);
		Boolean flags[] = new Boolean[SIZE + 1];
		Integer prime, k, iter, count;
		/*setTag(prime);
		setTag(k);
		setTag(iter);
		setTag(count);*/
		Integer iterations = 0;
		setTag(iterations);
		Double seconds = 0.0;
		setTag(seconds);
		Integer score = 0;
		setTag(score);
		Long startTime;
		Long elapsedTime = 0L;
		setTag(elapsedTime);

		startTime = System.currentTimeMillis();
		setTag(startTime);
		while (true) {
			count = 0;
			setTag(count);
			for (Integer i = 0; i <= SIZE; i++){
				setTag(i);
				flags[i] = true;
				
				//LOOF
				countLOOFDealocations("[LOOF] i-Integer (BB 5)");
				if (!result1Deallocated){
					countLOOFDealocations("[LOOF] result1-String (BB 5)");
					countLOOFDealocations("[LOOF] result2-String (BB 5)");
					countLOOFDealocations("[LOOF] iterations-Integer (BB 5)");
					countLOOFDealocations("[LOOF] seconds-Integer (BB 5)");
					countLOOFDealocations("[LOOF] score-Integer (BB 5)");
					countLOOFDealocations("[LOOF] startTime-Integer (BB 5)");
					countLOOFDealocations("[LOOF] count-Integer (BB 5)");
					
					result1Deallocated = true;
				}
			}
			
			for (Integer i = 0; i <= SIZE; i++) {
				setTag(i);
				if (flags[i]) {
					prime = i + i + 3;
					setTag(prime);
					for (k = i + prime; k <= SIZE; k += prime){
						setTag(k);
						flags[k] = false;
					}
					count++;
					setTag(count);
					countLOOFDealocations("[LOOF] prime-Integer (BB 13)");
					countLOOFDealocations("[LOOF] k-Integer (BB 13)");
				}
			}
			iterations++;
			setTag(iterations);
			elapsedTime = System.currentTimeMillis() - startTime;
			setTag(elapsedTime);
			if (elapsedTime >= 10000){
				break;
			}

			if (!result1Deallocated){
				countLOOFDealocations("[LOOF] score-Integer (BB 20)");
			}
			//countLOOFDealocations("[LOOF] elapsedTime-Integer (BB 20)");
		}
		seconds = elapsedTime / 1000.0;
		setTag(seconds);
		score = (int) Math.round(iterations / seconds);
		setTag(score);
		results1 = iterations + " iterations in " + seconds + " seconds";
		countLOOFDealocations("[LOOF] SIZE-String (BB 21)");
		countLOOFDealocations("[LOOF] startTime-Integer (BB 21)");
		setTag(results1);
		if (count != 1899){
			results2 = "Error: count <> 1899";
			countLOOFDealocations("[LOOF] iterations-Integer (BB 23)");
			countLOOFDealocations("[LOOF] seconds-Integer (BB 23)");
			countLOOFDealocations("[LOOF] score-Integer (BB 23)");
			countLOOFDealocations("[LOOF] count-Integer (BB 23)");
			countLOOFDealocations("[LOOF] elapsedTime-Integer (BB 23)");
			setTag(results2);
		}
		else{
			results2 = "Sieve score = " + score;
			countLOOFDealocations("[LOOF] iterations-Integer (BB 22)");
			countLOOFDealocations("[LOOF] seconds-Integer (BB 22)");
			countLOOFDealocations("[LOOF] count-Integer (BB 22)");
			countLOOFDealocations("[LOOF] elapsedTime-Integer (BB 22)");
			setTag(results2);
		}

		countLOOFDealocations("[LOOF] score-Integer (BB 25)");

		System.out.println(results1);
		System.out.println(results2);
	}

	public static void main(String[] args){

		teste();
		System.out.println("Quantidade de desalocações LOOF:"+ countDealocationsLOOF);


	}


	public static void countLOOFDealocations(String text){
    	//System.out.println(text);a
    	
    	countDealocationsLOOF = countDealocationsLOOF + 1;
    }
}
