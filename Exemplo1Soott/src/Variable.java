
public class Variable {
	
	private String name;
	private String type;
	
	
	public Variable(String name, String type) {
		super();
		this.name = name;
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (!(obj instanceof Variable))
            return false;
        if (obj == this)
            return true;

        Variable rhs = (Variable) obj;
        
        if (rhs.getName().equals(this.name) && rhs.getType().equals(this.type)){
        	return true;
        }else{
        	return false;
        }
 

	}
	
	public boolean isArray(){
		return type.contains("[]");
	}

}
